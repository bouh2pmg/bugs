#!/bin/bash

SRC="/go/src/gitlab.com/epitest/bugs"

docker run --rm -v $PWD:$SRC -w $SRC golang:1.7 "$@"
