#!groovy

pipeline {
    agent any

    options {
        buildDiscarder(logRotator(daysToKeepStr:'15'))
    }

    tools {
        go 'go-1.8.1'
    }

    parameters {
        booleanParam(name: 'RELEASE_BUILD', description: 'Start a release procedure ?', defaultValue: false)
    }

    stages {
        stage('Configuration')
        {
            steps {
                ansiColor('xterm')
                {
                    dir("go") {
                        withEnv(["GOPATH=$WORKSPACE/go/"]) {
                            sh "go get github.com/tebeka/go2xunit"                        
                        }
                    }
                    dir("go/src/github.com/Epitech/bugs") {
                        checkout scm
                        sh "git config user.email \"edex.argos@epitest.eu\" && git config user.name \"Jenkins\" && git config push.default simple"
                    }
                }
            }
        }
        stage('Pre-Release')
        {
            when {
                expression {
                    return params.RELEASE_BUILD
                }
            }
            steps {
                ansiColor('xterm')
                {
                    dir("go/src/github.com/Epitech/bugs") {
                        script {
                            env.GIT_URL = sh(returnStdout: true, script: 'git config --get remote.origin.url').trim()
                        }
                        library 'jenkins-release'
                        script {
                            env.current_version = getProjectVersion('version.go')
                        }
                        releaseVersion('version.go')
                        echo "Start a release for version ${env.release}."
                    }
                }
            }
        }
        stage('Build') {
            steps {
                ansiColor('xterm') {
                    dir("go/src/github.com/Epitech/bugs") {
                        withEnv(["GOPATH=$WORKSPACE/go/"]) {
                            sh "go build"                    
                        }
                    }
                }
            }
        }
        stage('Tests')
        {
            steps {
                ansiColor('xterm') {
                    dir("go/src/github.com/Epitech/bugs") {
                        withEnv(["GOPATH=$WORKSPACE/go/", "PATH+GO=$WORKSPACE/go/bin"]) {
                            sh "go test -v ./... | go2xunit -output tests.xml"
                        }
                    }
                }
            }
        }
        stage('Archive')
        {
            steps {
                ansiColor('xterm') {
                    dir("go/src/github.com/Epitech/bugs") {
                        junit allowEmptyResults: true, testResults: '**/tests.xml'                    
                    }
                }
            }
        }
        stage('Release')
        {
            when {
                expression {
                    return params.RELEASE_BUILD
                }
            }
            steps {
                ansiColor('xterm')
                {
                    sshagent(credentials: ['edex-argos.github.com'])
                    {
                        dir("go/src/github.com/Epitech/bugs") {
                            sh "git remote set-url --push origin git@github.com:Epitech/bugs.git"
                            generateChangelog()
                            tagAndPush()
                            bumpVersion('version.go')
                            script {
                                if (env.BRANCH_NAME.equalsIgnoreCase("master"))
                                {
                                    retry(10) {
                                        bumpMinor('version.go')
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    post {
        success {
            withCredentials([string(credentialsId: 'teams-webhook', variable: 'TEAMS_WEBHOOK')]) {
                script {
                    office365ConnectorSend message: "Build Success for $JOB_NAME#$BUILD_ID", status:"Success", webhookUrl:"$TEAMS_WEBHOOK"
                }
            }
        }
        failure {
            withCredentials([string(credentialsId: 'teams-webhook', variable: 'TEAMS_WEBHOOK')]) {
                script {
                    office365ConnectorSend message: "Build Failure for $JOB_NAME#$BUILD_ID", status:"Failure", webhookUrl:"$TEAMS_WEBHOOK"
                }
            }

        }
        unstable {
            withCredentials([string(credentialsId: 'teams-webhook', variable: 'TEAMS_WEBHOOK')]) {
                script {
                    office365ConnectorSend message: "Build Success but some tests failed for $JOB_NAME#$BUILD_ID", status:"Unstable", webhookUrl:"$TEAMS_WEBHOOK"
                }
            }

        }
        always {
            deleteDir()
        }
    }
}
