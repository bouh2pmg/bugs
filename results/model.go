package results

const (
	TestRunPublic   string = "public"
	TestRunInternal string = "internal"
)

type (
	Test struct {
		Name string `json:"name"`
	}

	Skill struct {
		Name  string  `json:"name"`
		Tests []*Test `json:"tests"`
	}

	ProjectInstance struct {
		Code        string `json:"code"`
		ModuleCode  string `json:"moduleCode"`
		ProjectSlug string `json:"projectSlug"`
		City        string `json:"city"`
		Year        int    `json:"year"`
		Name        string `json:"projectName"`
	}

	Project struct {
		Slug   *string  `json:"slug"`
		Name   string   `json:"name"`
		Skills []*Skill `json:"skills"`
		Module *Module  `json:"-"`
	}

	Module struct {
		Code     string     `json:"code"`
		Projects []*Project `json:"projects"`
	}

	TestRunItem struct {
		Name      string  `json:"name"`
		Passed    bool    `json:"passed"`
		Crashed   bool    `json:"crashed"`
		Mandatory bool    `json:"mandatory"`
		Comment   *string `json:"comment",omitempty`
	}

	TestRunExternalItem struct {
		Type    string  `json:"type"`
		Value   float32 `json:"value"`
		Comment *string `json:"comment",omitempty`
	}

	TestRunSkill struct {
		Name       string         `json:"name"`
		TraceTypes []string       `json:"traceTypes",omitempty`
		Tests      []*TestRunItem `json:"tests",omitempty`
	}

	ProjectGroup struct {
		Leader  string   `json:"leader"`
		Members []string `json:"members"`
	}

	TestRun struct {
		Instance      *ProjectInstance       `json:"instance"`
		Date          string                 `json:"date"`
		Visibility    string                 `json:"visibility"`
		Group         *ProjectGroup          `json:"group"`
		RunType       *string                `json:"type"`
		Skills        []*TestRunSkill        `json:"skills"`
		ExternalItems []*TestRunExternalItem `json:"externalItems"`
	}
)

func (ts *TestRunSkill) FailureCount() (failed int, total int) {
	total = len(ts.Tests)
	for _, t := range ts.Tests {
		if !t.Passed {
			failed += 1
		}
	}

	return
}

func TestRunVisibility(isPublic bool) string {
	if isPublic {
		return TestRunPublic
	}
	return TestRunInternal
}
