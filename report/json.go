package report

import (
	"encoding/json"
	"os"

	"github.com/Epitech/bugs/results"
)

type (
	JsonReporter struct {
	}
)

func (rep *JsonReporter) fileName(prefix string) string {
	return prefix + ".json"
}

func (rep *JsonReporter) WriteTestRun(testRun *results.TestRun) error {
	return rep.writeJson(testRun, rep.fileName("trace"))
}

func (rep *JsonReporter) WriteProject(report *results.Project) error {
	return rep.writeJson(report, rep.fileName("project"))
}

func (rep *JsonReporter) writeJson(obj interface{}, file string) error {
	bs, err := json.Marshal(obj)
	if err != nil {
		return err
	}

	f, err := os.OpenFile(file, os.O_RDWR|os.O_CREATE|os.O_EXCL, 0600)
	if err != nil {
		return err
	}

	defer f.Close()

	_, err = f.Write(bs)

	if err != nil {
		return err
	}

	return nil
}

func (rep *JsonReporter) RequireMetadata() bool {
	return true
}
