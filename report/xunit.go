package report

import (
	"encoding/xml"

	"github.com/Epitech/bugs/results"
)

type (
	xUnitData struct {
		XMLName    xml.Name         `xml:"testsuites"`
		TestSuites []xUnitTestSuite `xml:"testsuite"`
	}

	xUnitTestSuite struct {
		Name      string          `xml:"name,attr"`
		Tests     int             `xml:"tests,attr"`
		Errors    int             `xml:"errors,attr"`
		Failures  int             `xml:"failures,attr"`
		Skip      int             `xml:"skip,attr"`
		TestCases []xUnitTestCase `xml:"testcase"`
	}

	xUnitTestCase struct {
		Classname string        `xml:"classname,attr"`
		Name      string        `xml:"name,attr"`
		Time      float32       `xml:"time,attr"`
		Failure   *xUnitFailure `xml:"failure,omitempty"`
	}

	xUnitFailure struct {
		Type    string `xml:"type,attr"`
		Message string `xml:"message,attr"`
		Log     string `xml:",chardata"`
	}

	xUnitReporter struct {
		writer reportSink
	}
)

func XUnitReporter() Reporter {
	return AsAnonymousReporter(&xUnitReporter{fileSink("tests_report.xml")})
}

func toXTest(t *results.TestRunItem) xUnitTestCase {
	xtc := xUnitTestCase{Name: t.Name}
	if !t.Passed {
		var comment string
		if t.Comment == nil {
			comment = ""
		} else {
			comment = *t.Comment
		}

		xtc.Failure = &xUnitFailure{
			Message: comment,
			Type:    "failed",
		}
		if t.Crashed {
			xtc.Failure.Type = "signal"
		}
	}
	return xtc
}

func toXTSuite(ts *results.TestRunSkill) xUnitTestSuite {
	f, t := ts.FailureCount()
	xts := xUnitTestSuite{
		Name:     ts.Name,
		Tests:    t,
		Failures: f,
	}
	for _, t := range ts.Tests {
		xtc := toXTest(t)
		xtc.Classname = ts.Name
		xts.TestCases = append(xts.TestCases, xtc)
	}
	return xts
}

func toXUnit(tr *results.TestRun) (xdata xUnitData) {
	for _, ts := range tr.Skills {
		xdata.TestSuites = append(xdata.TestSuites, toXTSuite(ts))
	}
	return
}

func (r *xUnitReporter) WriteTestRun(tr *results.TestRun) error {
	if out, err := r.writer(); err != nil {
		return err
	} else if out != nil {
		defer out.Close()

		xr := toXUnit(tr)

		raw, err := xml.MarshalIndent(xr, "", "  ")
		if err != nil {
			return err
		}

		_, werr := out.Write(raw)
		return werr
	}

	return nil
}
