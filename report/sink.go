package report

import (
	"fmt"
	"io"
	"os"
)

const (
	openFileSinkRetries = 5
)

type (
	// TODO lift the "() ->" part into interface?
	reportSink func() (io.WriteCloser, error)

	FdWrapper struct {
		fd *os.File
	}
)

func fileSink(path string) reportSink {
	return func() (io.WriteCloser, error) {
		for i := 0; i < openFileSinkRetries; i++ {
			f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_EXCL, 0600)
			if err == nil {
				return f, nil
			}
			os.Remove(path)
		}
		return nil, fmt.Errorf("Could not create output %s", path)
	}
}

func fdSink(w *os.File) reportSink {
	return func() (io.WriteCloser, error) {
		return FdWrapper{w}, nil
	}
}

func nilSink() reportSink {
	return func() (io.WriteCloser, error) {
		return nil, nil
	}
}

/*
func writeTo(s reportSink, d string) error {
	w, err := s()
	if err != nil {
		return err
	}

	if w == nil {
		return nil
	}

	defer w.Close()

	_, werr := w.Write([]byte(d))
	return werr
}
*/

func (s FdWrapper) Write(p []byte) (int, error) {
	return s.fd.Write(p)
}

func (s FdWrapper) Close() error {
	return nil
}
