package report

import (
	"errors"
	"fmt"

	"github.com/Epitech/bugs/results"
)

type (
	Report struct {
		MetadataError error
		Project       *results.Project
		TestRun       *results.TestRun
	}

	Reporter interface {
		Write(report *Report) error
	}

	anonymousReporter struct {
		self AnonymousReporter
	}

	AnonymousReporter interface {
		WriteTestRun(testRun *results.TestRun) error
	}

	apiReporter struct {
		self ApiReporter
	}

	ApiReporter interface {
		WriteProject(report *results.Project) error

		WriteTestRun(report *results.TestRun) error

		RequireMetadata() bool
	}
)

func AsAnonymousReporter(r AnonymousReporter) Reporter {
	return &anonymousReporter{self: r}
}

func (r *anonymousReporter) Write(report *Report) error {
	return r.self.WriteTestRun(report.TestRun)
}

func AsApiReporter(r ApiReporter) Reporter {
	return &apiReporter{self: r}
}

func (r *apiReporter) Write(report *Report) error {

	if r.self.RequireMetadata() && report.MetadataError != nil {
		return errors.New(
			fmt.Sprintf("Missing metadata : %s", report.MetadataError.Error()))
	}

	if err := r.self.WriteProject(report.Project); err != nil {
		return err
	}

	if err := r.self.WriteTestRun(report.TestRun); err != nil {
		return err
	}

	return nil
}
