package cmd_aux

import (
	s "syscall"
)

const (
	AsStudent = iota
	AsTestRunner
)

type (
	RunnerConfigMock struct {
		verbose              bool
		testRunnerCredential *s.Credential
		studentCredential    *s.Credential
		CallsRecord          []uint
	}
)

func MakeRunnerConfig(verbose bool,
	testRunnerCredential, studentCredential *s.Credential) *RunnerConfigMock {

	return &RunnerConfigMock{
		verbose:              verbose,
		testRunnerCredential: testRunnerCredential,
		studentCredential:    studentCredential,
	}
}

func MockRunnerConfig(verbose bool) *RunnerConfigMock {
	return MakeRunnerConfig(verbose, nil, nil)
}

func QuietMockRunnerConfig() *RunnerConfigMock {
	return MockRunnerConfig(false)
}

func (r *RunnerConfigMock) StudentCredential() *s.Credential {
	r.CallsRecord = append(r.CallsRecord, AsStudent)

	return r.studentCredential
}

func (r *RunnerConfigMock) TestRunnerCredential() *s.Credential {
	r.CallsRecord = append(r.CallsRecord, AsTestRunner)

	return r.testRunnerCredential
}

func (r *RunnerConfigMock) Verbose() bool {
	return r.verbose
}
