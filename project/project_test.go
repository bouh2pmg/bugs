package project

import (
	"fmt"
	"testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"

	"github.com/Epitech/bugs/cmd"
	"github.com/Epitech/bugs/cmd_aux"
	ttg "github.com/Epitech/bugs/testing"
)

type testData struct {
	name                 string
	mandatory            bool
	ignoreStatus         bool
	timeout              time.Duration
	allowTimeout         bool
	useStudentCredential bool
}

func checkTests(tsl []*ttg.Suite, expected map[string][]testData) {
	for _, ts := range tsl {
		td, ok := expected[ts.Name]
		So(ok, ShouldBeTrue)
		So(len(ts.Tests), ShouldEqual, len(td))
		for i, got := range ts.Tests {
			So(got.TestName(), ShouldEqual, td[i].name)
			So(got.TestMandatory(), ShouldEqual, td[i].mandatory)
			So(got.IgnoreStatus(), ShouldEqual, td[i].ignoreStatus)
			So(got.CommandTimeout(), ShouldEqual, td[i].timeout)
			So(got.AllowTimeout(), ShouldEqual, td[i].allowTimeout)
			So(got.UseStudentCredential(), ShouldEqual, td[i].useStudentCredential)
		}
	}
}

func TestProject(t *testing.T) {
	dataOK := map[string][]testData{
		"ids": []testData{
			{
				name:      "id1",
				mandatory: true,
				timeout:   cmd.DefaultTimeout(),
			},
			{
				name:      "id2",
				mandatory: true,
				timeout:   cmd.DefaultTimeout(),
			},
		},
		"again": []testData{
			{
				name:    "di1",
				timeout: cmd.DefaultTimeout(),
			},
		},
	}

	Convey("It should execute test and report correct results", t, func() {

		cfg := cmd_aux.QuietMockRunnerConfig()

		p := New("_test/mandatoryOK")
		checkTests(p.Suites, dataOK)
		_, rep := p.Test(cfg)
		fmt.Printf("%v\n", rep)

		So(len(rep.Skills), ShouldEqual, 2)

		Convey("In the first suite", func() {
			suite := rep.Skills[0]

			So(suite.Name, ShouldEqual, "again")
			So(len(suite.Tests), ShouldEqual, 1)

			Convey("In the first test", func() {

				test := suite.Tests[0]

				So(test.Name, ShouldEqual, "di1")
				So(test.Passed, ShouldBeFalse)
				So(test.Crashed, ShouldBeFalse)
				So(test.Mandatory, ShouldBeFalse)
				So(*test.Comment, ShouldEqual, "Test failure: Expected exit status 10, got 0.")

			})

		})

		Convey("In the second suite", func() {
			suite := rep.Skills[1]

			So(suite.Name, ShouldEqual, "ids")
			So(len(suite.Tests), ShouldEqual, 2)

			Convey("In the first test", func() {

				test := suite.Tests[0]

				So(test.Name, ShouldEqual, "id1")
				So(test.Passed, ShouldBeTrue)
				So(test.Crashed, ShouldBeFalse)
				So(test.Mandatory, ShouldBeTrue)
				So(*test.Comment, ShouldEqual, "PASSED")

			})

			Convey("In the second test", func() {

				test := suite.Tests[1]

				So(test.Name, ShouldEqual, "id2")
				So(test.Passed, ShouldBeTrue)
				So(test.Crashed, ShouldBeFalse)
				So(test.Mandatory, ShouldBeTrue)
				So(*test.Comment, ShouldEqual, "PASSED")

			})
		})

	})

	dataKO := map[string][]testData{
		"ids": []testData{
			{
				name:      "id1",
				mandatory: true,
				timeout:   cmd.DefaultTimeout(),
			},
			{
				name:      "id2",
				mandatory: true,
				timeout:   cmd.DefaultTimeout(),
			},
		},
		"again": []testData{
			{
				name:      "di1",
				mandatory: true,
				timeout:   cmd.DefaultTimeout(),
			},
		},
	}
	Convey("It should execute test and report correct results", t, func() {

		cfg := cmd_aux.QuietMockRunnerConfig()

		p := New("_test/mandatoryKO")
		checkTests(p.Suites, dataKO)
		_, rep := p.Test(cfg)

		So(len(rep.Skills), ShouldEqual, 2)

		Convey("In the first suite", func() {
			suite := rep.Skills[0]

			So(suite.Name, ShouldEqual, "again")
			So(len(suite.Tests), ShouldEqual, 1)

			Convey("In the first test", func() {

				test := suite.Tests[0]

				So(test.Name, ShouldEqual, "di1")
				So(test.Passed, ShouldBeFalse)
				So(test.Crashed, ShouldBeFalse)
				So(test.Mandatory, ShouldBeTrue)
				So(*test.Comment, ShouldEqual, "Test failure: Expected exit status 10, got 0.")

			})

		})

		Convey("In the second suite", func() {
			suite := rep.Skills[1]

			So(suite.Name, ShouldEqual, "ids")
			So(len(suite.Tests), ShouldEqual, 2)

			Convey("In the first test", func() {

				test := suite.Tests[0]

				So(test.Name, ShouldEqual, "id1")
				So(test.Passed, ShouldBeTrue)
				So(test.Crashed, ShouldBeFalse)
				So(test.Mandatory, ShouldBeTrue)
				So(*test.Comment, ShouldEqual, "PASSED")

			})

			Convey("In the second test", func() {

				test := suite.Tests[1]

				So(test.Name, ShouldEqual, "id2")
				So(test.Passed, ShouldBeTrue)
				So(test.Crashed, ShouldBeFalse)
				So(test.Mandatory, ShouldBeTrue)
				So(*test.Comment, ShouldEqual, "PASSED")

			})
		})

	})
}

func TestNewHelpers(t *testing.T) {
	data := map[string][]testData{
		"Label first suite": []testData{
			{
				name:      "diff1",
				mandatory: true,
				timeout:   cmd.DefaultTimeout(),
			},
			{
				name:    "diff2",
				timeout: cmd.DefaultTimeout(),
			},
			{
				name:    "output1",
				timeout: cmd.DefaultTimeout(),
			},
			{
				name:      "signal1",
				mandatory: true,
				timeout:   cmd.DefaultTimeout(),
			},
			{
				name:      "status1",
				mandatory: true,
				timeout:   cmd.DefaultTimeout(),
			},
			{
				name:    "status2",
				timeout: cmd.DefaultTimeout(),
			},
		},
		"Label second suite": []testData{
			{
				name:    "",
				timeout: cmd.DefaultTimeout(),
			},
		},
	}
	Convey("It should load helpers correctly", t, func() {
		p := New("_test/good")
		checkTests(p.Suites, data)
	})
}

func TestNewProjectSortsSkillsByName(t *testing.T) {
	Convey("A project", t, func() {

		p := New("_test/skillSort")

		Convey("Should have its skills sorted by name", func() {

			expected := []string{
				"a: Suite#3",
				"b: Suite#1",
				"c: Suite#2",
			}

			actual := []string{}

			for _, s := range p.Suites {
				actual = append(actual, s.Name)
			}

			So(actual, ShouldResemble, expected)

		})

	})
}

func TestNewProjectWithHierarchicalOptions(t *testing.T) {
	Convey("A project with hierarchical options", t, func() {
		p := New("_test/test_options")

		Convey("Should have its options copied down", func() {

			expected := map[string][]testData{
				"skill a": []testData{
					{
						name:                 "test 1",
						mandatory:            false,
						ignoreStatus:         false,
						timeout:              time.Duration(1) * time.Second,
						allowTimeout:         true,
						useStudentCredential: true,
					},
					{
						name:                 "test 2",
						mandatory:            false,
						ignoreStatus:         false,
						timeout:              time.Duration(12) * time.Second,
						allowTimeout:         false,
						useStudentCredential: false,
					},
				},
				"skill b": []testData{
					{
						name:                 "test 3",
						mandatory:            false,
						ignoreStatus:         true,
						timeout:              time.Duration(1) * time.Minute,
						allowTimeout:         false,
						useStudentCredential: false,
					},
				},
			}

			checkTests(p.Suites, expected)

		})

	})

}
