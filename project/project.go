package project

import (
	"errors"
	"log"
	"sort"
	"strings"
	"time"

	"github.com/Epitech/bugs/cmd"
	"github.com/Epitech/bugs/options"
	"github.com/Epitech/bugs/results"
	"github.com/Epitech/bugs/testing"
)

type (
	Project struct {
		manifest      Manifest
		Name          string
		Suites        []*testing.Suite
		Metadata      *options.TestRunMetadata
		MetadataError error
	}
)

func New(testDir string) *Project {
	man := MustLoadManifest(testDir)
	var suites []*testing.Suite
	for label, skill := range man.Skills {
		suite := testing.NewTestSuite(label, skill, man.TestOptions)
		suites = append(suites, suite)
	}

	sort.Sort(testSuites(suites))

	meta, merr := options.GetMetadata()

	return &Project{
		man,
		man.Name,
		suites,
		meta,
		merr,
	}
}

func (p *Project) getProjectInstance() *results.ProjectInstance {
	meta := p.Metadata

	if meta == nil {
		return nil
	}

	return &results.ProjectInstance{
		meta.InstanceCode,
		meta.Module,
		meta.Slug,
		meta.City,
		meta.Year,
		p.Name,
	}
}

func (p *Project) getProjectLogins() *results.ProjectGroup {
	if p.Metadata == nil {
		return nil
	}

	logins := p.Metadata.Logins
	if len(logins) == 0 {
		return nil
	}

	return &results.ProjectGroup {
		logins[0],
		logins,
	}
}

func (p *Project) getRunType() *string {
	if p.Metadata == nil {
		return nil
	}

	return &p.Metadata.Type
}

func (p *Project) Test(cfg cmd.RunnerConfig) (error, *results.TestRun) {

	skills := []*results.TestRunSkill{}
	errs := []string{}

	for _, suite := range p.Suites {
		if err, trs := suite.Run(cfg); err != nil {
			errs = append(errs, err.Error())
		} else if trs != nil {
			skills = append(skills, trs)
		}
	}

	if len(errs) != 0 {
		return errors.New(strings.Join(errs, "\n")), nil
	}

	externalResults := externalItems()

	tr := results.TestRun{
		p.getProjectInstance(),
		time.Now().Format(time.RFC3339),
		results.TestRunVisibility(ReadIsTestRunPublic()),
		p.getProjectLogins(),
		p.getRunType(),
		skills,
		externalResults,
	}

	return nil, &tr
}

// TODO REVIEW extract functions plz
func externalItems() []*results.TestRunExternalItem {
	externalItems := []*results.TestRunExternalItem{}

	// coding style major
	{
		lineCount, logComment := ReadFileAndCountLines("reports/style-major.txt")
		normItem := &results.TestRunExternalItem{
			"lint.major",
			lineCount,
			&logComment,
		}
		externalItems = append(externalItems, normItem)
	}

	// coding style minor
	{
		lineCount, logComment := ReadFileAndCountLines("reports/style-minor.txt")
		normItem := &results.TestRunExternalItem{
			"lint.minor",
			lineCount,
			&logComment,
		}
		externalItems = append(externalItems, normItem)
	}

	// coding style info
	{
		lineCount, logComment := ReadFileAndCountLines("reports/style-info.txt")
		normItem := &results.TestRunExternalItem{
			"lint.info",
			lineCount,
			&logComment,
		}
		externalItems = append(externalItems, normItem)
	}

	// coding style metrics
	{
		lineCount, logComment := ReadFileAndCountLines("reports/style-metrics.txt")
		normItem := &results.TestRunExternalItem{
			"lint.metrics",
			lineCount,
			&logComment,
		}
		externalItems = append(externalItems, normItem)
	}

	// coding style note
	{
		note, logComment := ReadCodingStyleNote("reports/style-note.txt")
		normItem := &results.TestRunExternalItem{
			"lint.note",
			note,
			&logComment,
		}
		externalItems = append(externalItems, normItem)
	}

	// trace-pool.txt
	{
		logComment := ReadFileOnly("trace-pool.txt")
		tracePoolItem := &results.TestRunExternalItem{
			"trace-pool",
			0,
			&logComment,
		}
		externalItems = append(externalItems, tracePoolItem)
	}

	// delivery
	if deliveryStatus, deliveryLog, err := ReadDelivery(); err == nil {
		if !deliveryStatus {
			deliveryItem := &results.TestRunExternalItem{
				"delivery-error",
				0,
				&deliveryLog,
			}
			externalItems = append(externalItems, deliveryItem)
		}
	} else {
		log.Printf("WARN: Could not read delivery status and log - %s\n", err.Error())
	}

	// make
	if makeStatus, makeLog, err := ReadMake(); err == nil {
		if !makeStatus {
			makeItem := &results.TestRunExternalItem{
				"make-error",
				0,
				&makeLog,
			}
			externalItems = append(externalItems, makeItem)
		}
	} else {
		log.Printf("WARN: Could not read make status and log - %s\n", err.Error())
	}

	// banned functions
	if count, comment, err := ReadBanned(); err == nil {
		funcsItem := &results.TestRunExternalItem{
			"banned",
			float32(count),
			&comment,
		}
		externalItems = append(externalItems, funcsItem)
	} else {
		log.Printf("NOTICE: Could not read banned functions - %s\n", err.Error())
	}

	// coverage
	if mainComment, branchesValue, branchesComment,
		linesValue, linesComment, err := ReadCoverage(); err == nil {
		coverageItem := &results.TestRunExternalItem{
			"coverage.main",
			0,
			&mainComment,
		}
		externalItems = append(externalItems, coverageItem)

		coverageItem = &results.TestRunExternalItem{
			"coverage.branches",
			float32(branchesValue),
			&branchesComment,
		}
		externalItems = append(externalItems, coverageItem)

		coverageItem = &results.TestRunExternalItem{
			"coverage.lines",
			float32(linesValue),
			&linesComment,
		}
		externalItems = append(externalItems, coverageItem)
	} else {
		log.Printf("WARN: Could not read coverage report - %s\n", err.Error())
	}

	return externalItems
}

type testSuites []*testing.Suite

func (ts testSuites) Len() int      { return len(ts) }
func (ts testSuites) Swap(i, j int) { ts[i], ts[j] = ts[j], ts[i] }
func (ts testSuites) Less(i, j int) bool {
	return ts[i].Name < ts[j].Name
}

func (p *Project) ToProject() *results.Project {

	var module *results.Module = nil
	var slug *string = nil
	if p.Metadata != nil {
		module = &results.Module{
			p.Metadata.Module,
			[]*results.Project{},
		}
		slug = &p.Metadata.Slug
	}

	dbp := &results.Project{
		slug,
		p.Name,
		make([]*results.Skill, len(p.Suites)),
		module,
	}

	for i, skill := range p.Suites {
		dbs := &results.Skill{
			skill.Name,
			make([]*results.Test, len(skill.Tests)),
		}

		for j, test := range skill.Tests {
			dbt := &results.Test{
				test.TestName(),
			}

			dbs.Tests[j] = dbt
		}

		dbp.Skills[i] = dbs
	}

	return dbp
}
