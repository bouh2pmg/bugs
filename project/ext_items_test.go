package project

import (
	. "github.com/smartystreets/goconvey/convey"

	"os"
	"testing"
)

func TestReadNorm(t *testing.T) {

	Convey("Reading the coding style when", t, func() {
		err := os.Mkdir("reports", 0755)
		So(err, ShouldBeNil)
		defer os.Remove("reports/")

		Convey("there is no coding style file should", func() {
			Convey("Return a 0 line count with empty log", func() {

				r, log := ReadFileAndCountLines("reports/style-major.txt")

				So(r, ShouldEqual, 0)
				So(log, ShouldEqual, "")

			})
		})

		Convey("there is an empty coding style file should", func() {
			err := os.Link("_test/reports/empty_report.txt", "reports/style-minor.txt")
			So(err, ShouldBeNil)
			defer os.Remove("reports/style-minor.txt")

			Convey("Return a 0 line count with empty log", func() {

				r, log := ReadFileAndCountLines("reports/style-minor.txt")

				So(r, ShouldEqual, 0)
				So(log, ShouldEqual, "")
			})
		})

		Convey("there is a coding style file with comments", func() {
			err := os.Link("_test/reports/style-info.txt", "reports/style-info.txt")
			So(err, ShouldBeNil)

			defer os.Remove("reports/style-info.txt")

			Convey("Return the line count and no error", func() {

				r, log := ReadFileAndCountLines("reports/style-info.txt")

				So(r, ShouldEqual, 4)
				So(log, ShouldEqual, "test.c:3:L1\ntest.c:5:L1\ntest.c:6:L1\ntest.c:7:L1\n")

			})
		})
	})

}

func TestReadBanned(t *testing.T) {

	Convey("Reading the banned functions file when", t, func() {
		Convey("it does not exist should", func() {
			Convey("Return a 0 mark with an error", func() {

				m, c, err := ReadBanned()

				So(m, ShouldEqual, 0)

				So(c, ShouldEqual, "")

				So(err.Error(), ShouldEqual, "open "+BannedFuncsFileName+": no such file or directory")

			})
		})

		/*

		   This test cannot seem to be made to work when running as root,
		   which happens when building in a golang docker container

		   		Convey("there is a bad banned functions file, should", func() {
		   			err := os.Link("_test/failFile.note", BannedFuncsFileName)
		   			So(err, ShouldBeNil)
		   			er := os.Chmod("_test/failFile.note", 0000)
		   			So(er, ShouldBeNil)

		   			defer os.Remove(BannedFuncsFileName)
		   			defer os.Chmod("_test/failFile.note", 0665)

		   			Convey("Return an error", func() {

		   				m, c, err := ReadBanned()

		   				So(m, ShouldEqual, 0)

		   				So(c, ShouldEqual, "")

		   				So(err.Error(), ShouldEqual, "open "+BannedFuncsFileName+": permission denied")

		   			})
		   		})


		*/
		Convey("there is an empty banned functions file, should", func() {
			err := os.Link("_test/empty.note", BannedFuncsFileName)
			So(err, ShouldBeNil)

			defer os.Remove(BannedFuncsFileName)

			Convey("Return an error", func() {

				m, c, err := ReadBanned()

				So(m, ShouldEqual, 0)

				So(c, ShouldEqual, "")

				So(err.Error(), ShouldEqual, "Empty "+BannedFuncsFileName+" file")

			})
		})

		Convey("there is a banned functions file with a wrong mark and comment, should", func() {
			err := os.Link("_test/failFile.note", BannedFuncsFileName)
			So(err, ShouldBeNil)

			defer os.Remove(BannedFuncsFileName)

			Convey("Return an error", func() {

				m, c, err := ReadBanned()

				So(m, ShouldEqual, 0)

				So(c, ShouldEqual, "")

				So(err.Error(), ShouldEqual, "Could not parse "+BannedFuncsFileName+": strconv.Atoi: parsing \"FAILED\": invalid syntax")

			})
		})

		Convey("there is a banned functions file with a mark and comment, should", func() {
			err := os.Link("_test/banned_functions.note", BannedFuncsFileName)
			So(err, ShouldBeNil)

			defer os.Remove(BannedFuncsFileName)

			Convey("Return the banned functions mark with commment and no error", func() {

				m, c, err := ReadBanned()

				So(m, ShouldEqual, -42)

				So(c, ShouldEqual, "printf not allowed")

				So(err, ShouldBeNil)

			})
		})
	})

}

func TestReadCoverage(t *testing.T) {

	Convey("Reading the coverage when", t, func() {
		Convey("there is no coverage file should", func() {
			Convey("return a 0 mark with an error", func() {

				mainCom, bVal, bCom, lVal, lCom, err := ReadCoverage()

				So(bVal, ShouldEqual, 0)
				So(lVal, ShouldEqual, 0)
				So(mainCom, ShouldEqual, "")
				So(bCom, ShouldEqual, "")
				So(lCom, ShouldEqual, "")

				So(err.Error(), ShouldEqual, "open reports/coverage_report.txt: no such file or directory")

			})
		})

		Convey("there is an empty coverage file should", func() {
			err := os.Mkdir("reports", 0755)
			So(err, ShouldBeNil)
			defer os.Remove("reports/")

			err = os.Link("_test/reports/empty_report.txt", "reports/coverage_report.txt")
			So(err, ShouldBeNil)
			defer os.Remove("reports/coverage_report.txt")

			Convey("return the coverage mark and no error", func() {

				mainCom, bVal, bCom, lVal, lCom, err := ReadCoverage()

				So(bVal, ShouldEqual, 0)
				So(lVal, ShouldEqual, 0)
				So(mainCom, ShouldEqual, "")
				So(bCom, ShouldEqual, "")
				So(lCom, ShouldEqual, "")

				So(err.Error(), ShouldEqual, "Empty file: reports/coverage_report.txt")

			})
		})

		Convey("there is a truncated coverage file should", func() {
			err := os.Mkdir("reports", 0755)
			So(err, ShouldBeNil)
			defer os.Remove("reports/")

			err = os.Link("_test/reports/trunc_report.txt", "reports/coverage_report.txt")
			So(err, ShouldBeNil)
			defer os.Remove("reports/coverage_report.txt")

			Convey("return an error", func() {

				mainCom, bVal, bCom, lVal, lCom, err := ReadCoverage()

				So(bVal, ShouldEqual, 0)
				So(lVal, ShouldEqual, 0)
				So(mainCom, ShouldEqual, "")
				So(bCom, ShouldEqual, "")
				So(lCom, ShouldEqual, "")

				So(err.Error(), ShouldEqual, "Invalid file: reports/coverage_report.txt")

			})
		})

		Convey("there is a wrong coverage file for lines should", func() {
			err := os.Mkdir("reports", 0755)
			So(err, ShouldBeNil)
			defer os.Remove("reports/")

			err = os.Link("_test/reports/wrong_lines_report.txt", "reports/coverage_report.txt")
			So(err, ShouldBeNil)
			defer os.Remove("reports/coverage_report.txt")

			Convey("return an error", func() {

				mainCom, bVal, bCom, lVal, lCom, err := ReadCoverage()

				So(bVal, ShouldEqual, 0)
				So(lVal, ShouldEqual, 0)
				So(mainCom, ShouldEqual, "")
				So(bCom, ShouldEqual, "")
				So(lCom, ShouldEqual, "")

				So(err.Error(), ShouldEqual, "Invalid file: reports/coverage_report.txt")

			})
		})

		Convey("there is a wrong coverage file for branches should", func() {
			err := os.Mkdir("reports", 0755)
			So(err, ShouldBeNil)
			defer os.Remove("reports/")

			err = os.Link("_test/reports/wrong_branches_report.txt", "reports/coverage_report.txt")
			So(err, ShouldBeNil)
			defer os.Remove("reports/coverage_report.txt")

			Convey("return an error", func() {

				mainCom, bVal, bCom, lVal, lCom, err := ReadCoverage()

				So(bVal, ShouldEqual, 0)
				So(lVal, ShouldEqual, 0)
				So(mainCom, ShouldEqual, "")
				So(bCom, ShouldEqual, "")
				So(lCom, ShouldEqual, "")

				So(err.Error(), ShouldEqual, "Invalid file: reports/coverage_report.txt")

			})
		})

		Convey("there is a valid coverage file should", func() {
			err := os.Mkdir("reports", 0755)
			So(err, ShouldBeNil)
			defer os.Remove("reports/")

			err = os.Link("_test/reports/coverage_report.txt", "reports/coverage_report.txt")
			So(err, ShouldBeNil)
			defer os.Remove("reports/coverage_report.txt")

			Convey("return the coverage mark and no error", func() {

				mainCom, bVal, bCom, lVal, lCom, err := ReadCoverage()

				So(float32(bVal), ShouldEqual, 20.4)
				So(float32(lVal), ShouldEqual, 20.2)
				const mComExpected = `------------------------------------------------------------------------------
                           GCC Code Coverage Report
Directory: /tmp/tmp_bsq-2017-baron_l
------------------------------------------------------------------------------
File                                       Lines    Exec  Cover   Missing
------------------------------------------------------------------------------
basics.c                                      18       0     0%   13,15,18,22-24,27,31-34,37,42-44,46-47,49
bsq.c                                         15       0     0%   13,20,22-23,25-35
check_validity.c                              44      33    75%   75,77,79-80,82,84-85,87,89-90,92
find_square.c                                 52       0     0%   13,15-18,21,26-28,30-31,33-35,37,39,42,47-48,50-51,53,55-56,58,60,62,65,72-75,77-78,80-81,83,85,88,94-100,102-107
freeall.c                                      7       0     0%   13,17-18,20-21,23-24
get_file.c                                    19       0     0%   13,18-19,21-24,26,29,34-37,39-40,42,44-45,47
get_next_line.c                               72       0     0%   14,18-23,26,32-49,52,58-72,75,80-88,90-91,93-94,97,104-113,115-116,118-120
sort_wdtb.c                                    7       0     0%   13,17-18,20-22,24
tests/main.c                                  18      18   100%   
------------------------------------------------------------------------------
TOTAL                                        252      51    20%
------------------------------------------------------------------------------`
				So(mainCom, ShouldEqual, mComExpected)
				So(bCom, ShouldEqual, "40 out of 196")
				So(lCom, ShouldEqual, "51 out of 252")
				So(err, ShouldBeNil)

			})
		})
	})

}
