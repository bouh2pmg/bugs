package project

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"regexp"
	"strconv"
	"strings"
)

const (
	BannedFuncsFileName    string = "banned_functions.note"
	DeliveryStatusFileName string = "delivery.status"
	DeliveryLogFileName    string = "delivery.log"
	MakeStatusFileName     string = "make.status"
	MakeLogFileName        string = "make.log"
	CoverageReportFileName string = "reports/coverage_report.txt"
	PublicTestRunFileName  string = ".bugs_public"
)

func ReadFileAndCountLines(fileName string) (lineCount float32, logComment string) {
	file, err := os.Open(fileName)
	lineCount = 0
	logComment = ""
	if err == nil {
		defer file.Close()
		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			lineCount++
			logComment += scanner.Text() + "\n"
		}
	}
	return
}

func ReadFileOnly(fileName string) (logComment string) {
	file, err := os.Open(fileName)
	logComment = ""
	if err == nil {
		defer file.Close()
		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			logComment += scanner.Text() + "\n"
		}
	}
	return
}

func ReadCodingStyleNote(fileName string) (note float32, logComment string) {
	file, err := os.Open(fileName)
	note = 0
	logComment = ""
	if err == nil {
		defer file.Close()
		scanner := bufio.NewScanner(file)
		scanner.Scan()
		f, er := strconv.ParseFloat(scanner.Text(), 32)
		if er == nil {
			note = float32(f)
		}
	}
	return
}

func ReadBanned() (int, string, error) {
	normFile, err := os.Open(BannedFuncsFileName)
	if err != nil {
		return 0, "", err
	}
	defer normFile.Close()

	scanner := bufio.NewScanner(normFile)
	scanner.Split(bufio.ScanLines)

	if !scanner.Scan() {
		return 0, "", fmt.Errorf("Empty %s file", BannedFuncsFileName)
	}
	banMark, err := strconv.Atoi(scanner.Text())
	if err != nil {
		return 0, "", fmt.Errorf("Could not parse %s: %s", BannedFuncsFileName, err.Error())
	}

	banCom := ""
	if scanner.Scan() {
		banCom = scanner.Text()
	}

	return banMark, banCom, nil
}

func ReadDelivery() (status bool, log string, err error) {
	statusFile, err := os.Open(DeliveryStatusFileName)
	if err != nil {
		return
	}
	defer statusFile.Close()

	scanner := bufio.NewScanner(statusFile)
	scanner.Split(bufio.ScanLines)

	if !scanner.Scan() {
		err = scanner.Err()
		if err == nil {
			err = fmt.Errorf("Empty file: %s", DeliveryStatusFileName)
		}
		return
	}
	status = scanner.Text() == "success"

	// On success, return earlier
	if status {
		return
	}
	// On failure, read the log file and return its content

	logData, err := ioutil.ReadFile(DeliveryLogFileName)
	if err != nil {
		return
	}
	log = string(logData)

	return
}

func ReadMake() (status bool, log string, err error) {
	statusFile, err := os.Open(MakeStatusFileName)
	if err != nil {
		return
	}
	defer statusFile.Close()

	scanner := bufio.NewScanner(statusFile)
	scanner.Split(bufio.ScanLines)

	if !scanner.Scan() {
		err = scanner.Err()
		if err == nil {
			err = fmt.Errorf("Empty file: %s", MakeStatusFileName)
		}
		return
	}
	status = scanner.Text() == "0"

	// On success, return earlier
	if status {
		return
	}
	// On failure, read the log file and return its content

	logData, err := ioutil.ReadFile(MakeLogFileName)
	if err != nil {
		return
	}
	log = string(logData)

	return
}

func extractValuesFromLine(line string, _type string) (value float64, comment string, err error) {
	regex, err := regexp.Compile(_type + `: [0-9]+\.[0-9]*\% \([0-9]+ out of [0-9]+\)`)
	if err != nil {
		err = fmt.Errorf("Can't compile %s regex", _type)
		return
	}

	if regex.MatchString(line) == false {
		err = fmt.Errorf("Invalid file: %s", CoverageReportFileName)
		return
	}

	// get %age of coverage in float
	tmpVal := strings.Split(line, " ")[1]
	value, err = strconv.ParseFloat(tmpVal[:len(tmpVal)-1], 32)
	// get comment from " (X out of Y)" to "X out of Y"
	comm := strings.Split(line, "%")[1][2:]
	comment = comm[:len(comm)-1]
	return
}

func ReadCoverage() (mainComment string, branchesValue float64, branchesComment string, linesValue float64, linesComment string, err error) {
	coverageFile, err := os.Open(CoverageReportFileName)
	if err != nil {
		return
	}
	defer coverageFile.Close()

	scanner := bufio.NewScanner(coverageFile)
	scanner.Split(bufio.ScanLines)

	tab := make([]string, 0)
	for scanner.Scan() {
		tab = append(tab, scanner.Text())
	}

	if len(tab) == 0 {
		err = fmt.Errorf("Empty file: %s", CoverageReportFileName)
		return
	}
	if len(tab) < 11 {
		err = fmt.Errorf("Invalid file: %s", CoverageReportFileName)
		return
	}

	if branchesValue, branchesComment, err = extractValuesFromLine(tab[len(tab)-1], "branches"); err != nil {
		return "", 0, "", 0, "", err
	}
	if linesValue, linesComment, err = extractValuesFromLine(tab[len(tab)-2], "lines"); err != nil {
		return "", 0, "", 0, "", err
	}

	epurTab := tab[:len(tab)-2]
	mainComment = strings.Join(epurTab, "\n")
	return
}

func ReadIsTestRunPublic() bool {
	if fi, err := os.Stat(PublicTestRunFileName); err == nil {
		return fi.Mode().IsRegular()
	}
	return false
}
