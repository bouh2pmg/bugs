package project

import (
	gotest "testing"

	. "github.com/smartystreets/goconvey/convey"

	"github.com/Epitech/bugs/testing"
)

func stringPtr(s string) *string {
	return &s
}

func boolPtr(b bool) *bool {
	return &b
}

var goodManifest = Manifest{
	Name: "Project",
	Skills: map[string]testing.Skill{
		"Label first suite": testing.Skill{
			Diff: []testing.DiffData{
				{
					TestData: testing.TestData{
						Name:      "diff1",
						Mandatory: true,
						Cmd:       "Command diff 1",
					},
					Expected: "Expected diff 1",
					Output:   "Output diff 1",
				},
				{
					TestData: testing.TestData{
						Name:      "diff2",
						Mandatory: false,
						Cmd:       "Command diff 2",
					},
					Expected: "Expected diff 2",
					Output:   "Output diff 2",
				},
			},
			Output: []testing.OutputData{
				{
					TestData: testing.TestData{
						Name: "output1",
						Cmd:  "Command output 1",
					},
					Expected: "Expected output 1",
				},
			},
			Signal: []testing.SignalData{
				{
					TestData: testing.TestData{
						Name:      "signal1",
						Mandatory: true,
						Cmd:       "Command signal 1",
					},
				},
			},
			Status: []testing.StatusData{
				{
					TestData: testing.TestData{
						Name:      "status1",
						Mandatory: true,
						Cmd:       "Command status 1",
					},
					Expected: 0,
				},
				{
					TestData: testing.TestData{
						Name: "status2",
						Cmd:  "Command status 2",
					},
					Expected: 1,
				},
			},
		},
		"Label second suite": testing.Skill{
			Diff: []testing.DiffData{
				{
					TestData: testing.TestData{
						Cmd: "Command diff 1",
					},
					Expected: "Expected diff 1",
					Output:   "Output diff 1",
				},
			},
		},
	},
}

var testOptionsManifest = Manifest{
	TestOptions: testing.TestOptions{
		NoStatusCheck: nil,
		Timeout:       stringPtr("12s"),
		SoftTimeout:   nil,
		RunAsStudent:  nil,
	},
	Name: "my_project",
	Skills: map[string]testing.Skill{
		"skill a": testing.Skill{
			TestOptions: testing.TestOptions{
				NoStatusCheck: nil,
				Timeout:       nil,
				SoftTimeout:   boolPtr(true),
				RunAsStudent:  nil,
			},
			Status: []testing.StatusData{
				{
					TestData: testing.TestData{
						TestOptions: testing.TestOptions{
							NoStatusCheck: nil,
							Timeout:       stringPtr("1s"),
							SoftTimeout:   nil,
							RunAsStudent:  boolPtr(true),
						},
						Name: "test 1",
						Cmd:  "touch test1",
					},
				},
				{
					TestData: testing.TestData{
						TestOptions: testing.TestOptions{
							NoStatusCheck: nil,
							Timeout:       nil,
							SoftTimeout:   boolPtr(false),
							RunAsStudent:  nil,
						},
						Name: "test 2",
						Cmd:  "touch test2",
					},
				},
			},
		},
		"skill b": testing.Skill{
			TestOptions: testing.TestOptions{
				NoStatusCheck: nil,
				Timeout:       stringPtr("1m"),
				SoftTimeout:   nil,
				RunAsStudent:  nil,
			},
			Status: []testing.StatusData{
				{
					TestData: testing.TestData{
						TestOptions: testing.TestOptions{
							NoStatusCheck: boolPtr(true),
							Timeout:       nil,
							SoftTimeout:   nil,
							RunAsStudent:  nil,
						},
						Name: "test 3",
						Cmd:  "touch test3",
					},
				},
			},
		},
	},
}

func TestManifest(t *gotest.T) {
	Convey("It should load a correct manifest", t, func() {
		m, err := LoadManifest("_test/good")
		So(err, ShouldBeNil)
		So(*m, ShouldResemble, goodManifest)
	})

	Convey("It should load a manifest with TestOptions at each level", t, func() {
		m, err := LoadManifest("_test/test_options")
		So(err, ShouldBeNil)
		So(*m, ShouldResemble, testOptionsManifest)
	})

	Convey("It should handle incorrect manifests", t, func() {
		_, err := LoadManifest("nope")
		So(err, ShouldNotBeNil)
		So(err.Error(), ShouldEqual, "open nope/manifest.json: no such file or directory")
	})

	Convey("It should handle a manifest with duplicate test name", t, func() {
		_, err := LoadManifest("_test/dupe")
		So(err, ShouldNotBeNil)

		So(err.Error(), ShouldEqual, `Duplicate test names:
  test 'dup1' in skill 'Label first suite'
  test 'dup2' in skill 'Label second suite'`)
	})

}
