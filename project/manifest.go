package project

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"path"
	"sort"
	"strings"

	"github.com/Epitech/bugs/testing"
)

const (
	ManifestName = "manifest.json"
)

type (
	Manifest struct {
		testing.TestOptions
		Name   string `json:"project"`
		Skills map[string]testing.Skill
	}

	duplicateTestError struct {
		Skill string
		Test  string
	}
)

func LoadManifest(dir string) (*Manifest, error) {
	raw, err := ioutil.ReadFile(path.Join(dir, ManifestName))
	if err != nil {
		return nil, err
	}
	var man Manifest

	if err = json.Unmarshal(raw, &man); err != nil {
		return nil, err
	}

	if err := validateTestNames(man); err != nil {
		return nil, err
	}

	return &man, err
}

func MustLoadManifest(dir string) Manifest {
	man, err := LoadManifest(dir)
	if err != nil {
		log.Fatalf("Failed to load manifest: %s\n", err)
	}

	return *man
}

func validateTestNames(m Manifest) error {

	errs := []duplicateTestError{}

	for skillName, skill := range m.Skills {

		testNames := []string{}

		for _, t := range skill.Diff {
			testNames = append(testNames, t.Name)
		}
		for _, t := range skill.Output {
			testNames = append(testNames, t.Name)
		}
		for _, t := range skill.Signal {
			testNames = append(testNames, t.Name)
		}
		for _, t := range skill.Status {
			testNames = append(testNames, t.Name)
		}

		uniqueNames := map[string]struct{}{}
		dupNames := map[string]struct{}{}

		for _, name := range testNames {

			if _, ok := uniqueNames[name]; ok {
				if _, d := dupNames[name]; !d {
					errs = append(errs, duplicateTestError{skillName, name})
					dupNames[name] = struct{}{}
				}
			} else {
				uniqueNames[name] = struct{}{}
			}
		}
	}

	if len(errs) == 0 {
		return nil
	}

	msgs := []string{}
	for _, dupError := range errs {
		msgs = append(msgs, fmt.Sprintf("  test '%s' in skill '%s'", dupError.Test, dupError.Skill))
	}

	sort.Strings(msgs)

	var msg string = "Duplicate test names:\n" + strings.Join(msgs, "\n")

	return errors.New(msg)
}
