[<img src="https://badges.epitest.eu/badge/Release/0.26.0/green">](https://docs.epitest.eu/)

All the useful documentation about `bugs` can be found in the [WIKI] (https://gitlab.com/epitest/bugs/wikis/home)
