package cmd

import (
	"fmt"
	"strconv"
	"strings"
	s "syscall"
)

type (
	RunnerConfig interface {
		StudentCredential() *s.Credential
		TestRunnerCredential() *s.Credential
		Verbose() bool
	}

	defaultRunnerConfig struct{}

	defaultGroupsRunnerConfig struct {
		studentUid    uint32
		testRunnerUid uint32
	}
)

var (
	defaultTestRunnerCredential *s.Credential = &s.Credential{
		Uid:    1000,
		Gid:    100,
		Groups: []uint32{1001},
	}

	defaultStudentCredential *s.Credential = &s.Credential{
		Uid:    1001,
		Gid:    1001,
		Groups: []uint32{},
	}

	runnerConfig RunnerConfig = &defaultRunnerConfig{}
)

func DefaultRunnerConfig() RunnerConfig {
	return runnerConfig
}

func (r *defaultRunnerConfig) StudentCredential() *s.Credential {
	return defaultStudentCredential
}

func (r *defaultRunnerConfig) TestRunnerCredential() *s.Credential {
	return defaultTestRunnerCredential
}

func (r *defaultRunnerConfig) Verbose() bool {
	return true
}

func DefaultGroupsRunnerConfig(uids string) (RunnerConfig, error) {
	uidArray := strings.Split(strings.Trim(uids, " "), ",")

	if len(uidArray) != 2 {
		return nil, fmt.Errorf("Invalid uids %s", uids)
	}

	testRunnerUid, err1 := strconv.ParseUint(uidArray[0], 10, 32)
	studentUid, err2 := strconv.ParseUint(uidArray[1], 10, 32)

	if err1 != nil || err2 != nil {
		return nil, fmt.Errorf("Invalid uids %s", uids)
	}

	return &defaultGroupsRunnerConfig{
		studentUid:    uint32(studentUid),
		testRunnerUid: uint32(testRunnerUid),
	}, nil
}

func (r *defaultGroupsRunnerConfig) StudentCredential() *s.Credential {
	return &s.Credential{
		Uid:    r.studentUid,
		Gid:    1001,
		Groups: []uint32{},
	}
}

func (r *defaultGroupsRunnerConfig) TestRunnerCredential() *s.Credential {
	return &s.Credential{
		Uid:    r.testRunnerUid,
		Gid:    100,
		Groups: []uint32{1001},
	}
}

func (r *defaultGroupsRunnerConfig) Verbose() bool {
	return true
}
