package cmd_test

import (
	"fmt"
	"testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"

	"github.com/Epitech/bugs/cmd"
)

func NewCmdSpecWithDefaults(cmdParts []string) cmd.CmdSpec {
	return cmd.CmdSpec{
		cmdParts,
		nil,
		nil,
		cmd.DefaultTimeout(),
		cmd.DefaultOutputLimit,
	}
}

func NewCmdLineWithDefaults(cmdLine string) cmd.CmdSpec {
	return cmd.NewCmdLine(cmdLine, nil, nil, cmd.DefaultTimeout())
}

func AReportIsReturned(cmdSpec cmd.CmdSpec, f func(*cmd.CmdReport)) {
	Convey("A command report is returned", func() {
		report, err := cmdSpec.Run(false)

		So(err, ShouldBeNil)
		f(report)
	})
}

func StatusIs(expected int, report *cmd.CmdReport) {
	Convey(fmt.Sprint("Its exit status is %d", expected), func() {
		So(report.Status, ShouldEqual, expected)
	})
}

func SignalIs(expected int, report *cmd.CmdReport) {
	Convey(fmt.Sprint("Its signal is %d", expected), func() {
		So(report.Signal, ShouldEqual, expected)
	})
}

func StdoutIs(expected string, report *cmd.CmdReport) {
	Convey(fmt.Sprintf("Its stdout is <%s>", expected), func() {
		So(report.Stdout, ShouldResemble, []byte(expected))
	})
}

func StderrIs(expected string, report *cmd.CmdReport) {
	Convey(fmt.Sprintf("Its stderr is <%s>", expected), func() {
		So(report.Stderr, ShouldResemble, []byte(expected))
	})
}

func ExecIsTimedout(report *cmd.CmdReport) {
	Convey("Execution is timed out", func() {
		So(report.TimedOut, ShouldBeTrue)
	})
}

func ExecIsNotTimedout(report *cmd.CmdReport) {
	Convey("Execution is not timed out", func() {
		So(report.TimedOut, ShouldBeFalse)
	})
}

func TestStatusCapture(t *testing.T) {

	Convey("Given a command that exits with status 0", t, func() {
		cmdSpec := NewCmdSpecWithDefaults([]string{"_test/mouli_subject", "exit", "0"})

		AReportIsReturned(cmdSpec, func(report *cmd.CmdReport) {
			StatusIs(0, report)
		})
	})
}

func TestNonZeroStatusCapture(t *testing.T) {

	Convey("Given a command that exits with status 1", t, func() {
		cmdSpec := NewCmdSpecWithDefaults([]string{"_test/mouli_subject", "exit", "1"})

		AReportIsReturned(cmdSpec, func(report *cmd.CmdReport) {
			StatusIs(1, report)
		})
	})
}

func TestSignalCapture(t *testing.T) {

	Convey("Given a command that raises a crashing signal", t, func() {
		cmdSpec := NewCmdSpecWithDefaults([]string{"_test/mouli_subject", "sig", "11"})

		AReportIsReturned(cmdSpec, func(report *cmd.CmdReport) {
			SignalIs(11, report)
		})
	})

}

func TestStdoutCapture(t *testing.T) {
	Convey("Given a command that prints to stdout", t, func() {

		cmdSpec := NewCmdSpecWithDefaults([]string{"_test/mouli_subject", "outs", "foo"})

		AReportIsReturned(cmdSpec, func(report *cmd.CmdReport) {
			StdoutIs("foo", report)
		})
	})
}

func TestStdoutCaptureWithExcess(t *testing.T) {

	Convey("Given a command that exceeds stdout limit ('01234567890' with limit 8)", t, func() {
		cmdSpec := cmd.CmdSpec{
			[]string{"_test/mouli_subject", "outs", "0123456789"},
			nil,
			[]byte{},
			cmd.DefaultTimeout(),
			8,
		}

		AReportIsReturned(cmdSpec, func(report *cmd.CmdReport) {
			StdoutIs("01234567", report)
		})
	})
}

func TestStderrCapture(t *testing.T) {

	Convey("Given a command that prints to stderr", t, func() {

		cmdSpec := NewCmdSpecWithDefaults([]string{"_test/mouli_subject", "errs", "bar"})

		AReportIsReturned(cmdSpec, func(report *cmd.CmdReport) {
			StderrIs("bar", report)
		})
	})
}

func TestStderrCaptureWithExcess(t *testing.T) {

	Convey("Given a command that exceeds stdeerr limit ('01234567890' with limit 8)", t, func() {
		cmdSpec := cmd.CmdSpec{
			[]string{"_test/mouli_subject", "errs", "0123456789"},
			nil,
			[]byte{},
			cmd.DefaultTimeout(),
			8,
		}

		AReportIsReturned(cmdSpec, func(report *cmd.CmdReport) {
			StderrIs("01234567", report)
		})
	})
}

func TestStdoutCaptureOnCrash(t *testing.T) {
	Convey("Given a command that prints to stdout and raises a crashing signal", t, func() {

		cmdSpec := NewCmdSpecWithDefaults([]string{"_test/mouli_subject", "outs", "foo", "sig", "6"})

		AReportIsReturned(cmdSpec, func(report *cmd.CmdReport) {
			SignalIs(6, report)

			StdoutIs("foo", report)
		})
	})
}

func TestStderrCaptureOnCrash(t *testing.T) {
	Convey("Given a command that prints to stderr and raises a crashing signal", t, func() {

		cmdSpec := NewCmdSpecWithDefaults([]string{"_test/mouli_subject", "errs", "bar", "sig", "6"})

		AReportIsReturned(cmdSpec, func(report *cmd.CmdReport) {
			SignalIs(6, report)

			StderrIs("bar", report)
		})
	})
}

func TestTimeoutCapture(t *testing.T) {
	Convey("Given a command that times out", t, func() {

		cmdSpec := cmd.NewCmdSpec(
			[]string{"_test/mouli_subject", "sleep", "1"},
			nil,
			nil,
			time.Duration(100)*time.Millisecond,
		)

		AReportIsReturned(cmdSpec, func(report *cmd.CmdReport) {

			ExecIsTimedout(report)

		})
	})
}

func TestStdoutCaptureOnTimeout(t *testing.T) {
	Convey("Given a command that prints to stdout and times out", t, func() {

		cmdSpec := cmd.NewCmdSpec(
			[]string{"_test/mouli_subject", "outs", "foo", "sleep", "1"},
			nil,
			nil,
			time.Duration(100)*time.Millisecond,
		)

		AReportIsReturned(cmdSpec, func(report *cmd.CmdReport) {
			ExecIsTimedout(report)
			StdoutIs("foo", report)
		})
	})
}

func TestStderrCaptureOnTimeout(t *testing.T) {
	Convey("Given a command that prints to stderr and times out", t, func() {

		cmdSpec := cmd.NewCmdSpec(
			[]string{"_test/mouli_subject", "errs", "bar", "sleep", "1"},
			nil,
			nil,
			time.Duration(100)*time.Millisecond,
		)

		AReportIsReturned(cmdSpec, func(report *cmd.CmdReport) {
			ExecIsTimedout(report)
			StderrIs("bar", report)
		})
	})
}

func TestNotTimeoutCapture(t *testing.T) {
	Convey("Given a command that runs long but does not time out", t, func() {

		cmdSpec := cmd.NewCmdSpec(
			[]string{"_test/mouli_subject", "sleep", "1"},
			nil,
			nil,
			time.Duration(2)*time.Second,
		)

		AReportIsReturned(cmdSpec, func(report *cmd.CmdReport) {
			ExecIsNotTimedout(report)
		})
	})
}

func TestInputGoesToCommand(t *testing.T) {
	Convey("Given a command that sends <hello> to 'cat'", t, func() {

		cmdSpec := cmd.NewCmdSpec(
			[]string{"/bin/cat"},
			nil,
			[]byte("hello"),
			cmd.DefaultTimeout(),
		)

		AReportIsReturned(cmdSpec, func(report *cmd.CmdReport) {
			StdoutIs("hello", report)
		})

	})
}

func TestCmdLine(t *testing.T) {

	Convey("A command made from parts", t, func() {

		cmdSpec := NewCmdSpecWithDefaults([]string{"cmd", "arg1", "arg2"})

		Convey("Should have the first part as executable", func() {
			So(cmdSpec.CmdParts[0], ShouldEqual, "cmd")
		})

		Convey("Should have the next parts as arguments", func() {
			So(cmdSpec.CmdParts[1:], ShouldResemble, []string{"arg1", "arg2"})
		})

	})

	Convey("A command made from parts with non-default input and timeout", t, func() {

		cmdSpec := cmd.NewCmdSpec([]string{"cmd", "arg1", "arg2"},
			nil,
			[]byte("input"),
			time.Duration(1)*time.Hour,
		)

		Convey("Should have the specified input", func() {
			So(cmdSpec.Input, ShouldResemble, []byte("input"))
		})

		Convey("Should have the specified timeout", func() {
			So(cmdSpec.Timeout, ShouldEqual, time.Duration(1)*time.Hour)
		})

	})

	Convey("A command made from a line", t, func() {

		cmdSpec := NewCmdLineWithDefaults("cmd arg1 arg2")

		Convey("Splits the line into executable and arguments", func() {
			So(cmdSpec.CmdParts[0], ShouldEqual, "cmd")
			So(cmdSpec.CmdParts[1:], ShouldResemble, []string{"arg1", "arg2"})
		})

	})

	Convey("A command made from a line containing quotes", t, func() {

		cmdSpec := NewCmdLineWithDefaults(`cmd "arg1 arg2"`)

		Convey("Splits the line into executable and arguments, keeping the quoted parts whole",
			func() {
				So(cmdSpec.CmdParts[0], ShouldEqual, "cmd")
				So(cmdSpec.CmdParts[1:], ShouldResemble, []string{"arg1 arg2"})
			})

	})

	Convey("A command made from a line with non-default input and timeout", t, func() {

		cmdSpec := cmd.NewCmdLine("cmd arg1 arg2",
			nil,
			[]byte("input"),
			time.Duration(1)*time.Hour,
		)

		Convey("Should have the specified input", func() {
			So(cmdSpec.Input, ShouldResemble, []byte("input"))
		})

		Convey("Should have the specified timeout", func() {
			So(cmdSpec.Timeout, ShouldEqual, time.Duration(1)*time.Hour)
		})

	})

}

func TestRunErrors(t *testing.T) {
	Convey("An empty command", t, func() {
		cmdSpec := NewCmdLineWithDefaults("")
		Convey("When run", func() {
			_, err := cmdSpec.Run(false)
			Convey("Returns a CmdError", func() {
				So(err, ShouldNotBeNil)

				cmdErr, ok := err.(cmd.CmdError)
				So(ok, ShouldBeTrue)

				So(cmdErr.IsClientError, ShouldBeTrue)
				So(cmdErr.Error(), ShouldEqual, "Invalid command: Empty command.")
			})
		})
	})

	Convey("A command for a missing executable", t, func() {
		cmdSpec := NewCmdLineWithDefaults("./thisexecutablesdoesnotexist")
		Convey("When run", func() {
			_, err := cmdSpec.Run(false)
			Convey("Returns a CmdError", func() {
				So(err, ShouldNotBeNil)

				cmdErr, ok := err.(cmd.CmdError)
				So(ok, ShouldBeTrue)

				So(cmdErr.IsClientError, ShouldBeFalse)
				So(cmdErr.Error(), ShouldEqual,
					"Command failure: fork/exec ./thisexecutablesdoesnotexist: no such file or directory")
			})
		})
	})
}

var splitCmdTests = []struct {
	in       string
	expected []string
}{
	{"ls", []string{"ls"}},
	{"ls -l toto", []string{"ls", "-l", "toto"}},
	{"echo 'hey you'", []string{"echo", "hey you"}},
	{"echo ''", []string{"echo", ""}},
	{"echo 'hey you' and you", []string{"echo", "hey you", "and", "you"}},
	{"echo \"hey you\"", []string{"echo", "hey you"}},
}

func TestSplitCmd(t *testing.T) {
	Convey("A command line splits its argument", t, func() {
		for _, it := range splitCmdTests {
			Convey(fmt.Sprintf("%s becomes %#v", it.in, it.expected), func() {
				got := NewCmdLineWithDefaults(it.in).CmdParts

				So(got, ShouldResemble, it.expected)
			})
		}
	})
}
