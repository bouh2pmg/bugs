package cmd

import (
	"bytes"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"strings"
	"syscall"
	"time"
	"unicode"
)

const (
	TimeFormat         string = "02/01/2006 15:04:05.999 -0700"
	DefaultOutputLimit uint   = 32768
)

var (
	defaultTimeout time.Duration = time.Duration(10) * time.Second
)

type (
	CmdSpec struct {
		CmdParts    []string
		Credential  *syscall.Credential
		Input       []byte
		Timeout     time.Duration
		OutputLimit uint
	}

	CmdReport struct {
		Stdout   []byte
		Stderr   []byte
		Status   uint8
		Signal   uint8
		TimedOut bool
	}

	startedCmd struct {
		cmd     *exec.Cmd
		out     *boundedBuffer
		err     *boundedBuffer
		pid     int
		timeout <-chan time.Time
	}

	CmdError struct {
		IsClientError bool
		Cause         error
	}

	boundedBuffer struct {
		capacity  uint
		available uint
		buf       *bytes.Buffer
	}
)

func DefaultTimeout() time.Duration {
	return defaultTimeout
}

func newBoundedBuffer(capacity uint) *boundedBuffer {
	return &boundedBuffer{capacity, capacity, new(bytes.Buffer)}
}

func (bw *boundedBuffer) Write(p []byte) (int, error) {
	n := uint(len(p))
	c := bw.available
	if c > n {
		c = n
	}
	w, err := bw.buf.Write(p[:c])
	bw.available -= uint(w)
	return int(n), err
}

func (bw *boundedBuffer) Bytes() []byte {
	return bw.buf.Bytes()
}

func (c CmdError) Error() string {
	var errorType = "Command failure"
	if c.IsClientError {
		errorType = "Invalid command"
	}
	return fmt.Sprintf("%s: %s", errorType, c.Cause.Error())
}

func NewCmdError(msg string) CmdError {
	return CmdError{true, errors.New(msg)}
}

func NewCmdSpec(cmdParts []string, cred *syscall.Credential, input []byte, timeout time.Duration) CmdSpec {
	return CmdSpec{cmdParts, cred, input, timeout, DefaultOutputLimit}
}

func NewCmdLine(cmdLine string, cred *syscall.Credential, input []byte, timeout time.Duration) CmdSpec {
	return NewCmdSpec(splitCmd(cmdLine), cred, input, timeout)
}

func splitCmd(str string) (ret []string) {
	lastQuote := rune(0)
	f := func(c rune) bool {
		switch {
		case c == lastQuote:
			lastQuote = rune(0)
			return false
		case lastQuote != rune(0):
			return false
		case unicode.In(c, unicode.Quotation_Mark):
			lastQuote = c
			return false
		default:
			return unicode.IsSpace(c)
		}
	}
	for _, it := range strings.FieldsFunc(str, f) {
		if unicode.In(rune(it[0]), unicode.Quotation_Mark) {
			it = it[1 : len(it)-1]
		}
		ret = append(ret, it)
	}
	return
}

func (spec CmdSpec) start(verbose bool) (*startedCmd, error) {
	// setup command executable, arguments
	execArgv := spec.CmdParts
	if len(execArgv) == 0 {
		return nil, NewCmdError("Empty command.")
	}

	var cmd *exec.Cmd = exec.Command(execArgv[0], execArgv[1:]...)

	cmd.SysProcAttr = &syscall.SysProcAttr{
		Setpgid:    true,
		Credential: spec.Credential,
	}

	// plug input if any

	cin, inerr := cmd.StdinPipe()
	if inerr != nil {
		return nil, CmdError{Cause: inerr}
	}

	// plug outputs
	cout := newBoundedBuffer(spec.OutputLimit)
	cmd.Stdout = cout

	cerr := newBoundedBuffer(spec.OutputLimit)
	cmd.Stderr = cerr

	// start
	if err := cmd.Start(); err != nil {
		return nil, CmdError{Cause: err}
	}

	pid := cmd.Process.Pid
	log(verbose, "Execution (PID %d) started at %s", pid, time.Now().Format(TimeFormat))

	timeout := time.After(spec.Timeout)

	if spec.Input != nil {
		defer cin.Close()
		_, werr := cin.Write(spec.Input)
		if werr != nil {
			log(verbose, "Failed to write to stdin of PID %d: %s", pid, werr.Error())
			killProcessGroup(pid)
			return nil, CmdError{Cause: werr}
		}
	}

	return &startedCmd{cmd, cout, cerr, pid, timeout}, nil
}

func (spec CmdSpec) waitCmd(verbose bool, sc *startedCmd) *CmdReport {
	var done chan error = make(chan error, 1)
	go func() {
		done <- sc.cmd.Wait()
	}()

	var res CmdReport

	// on complete
	select {

	case <-sc.timeout:
		killProcessGroup(sc.pid)
		log(verbose, "Execution (PID %d) timed out at %s", sc.pid, time.Now().Format(TimeFormat))
		res.TimedOut = true
		<-done

	// TODO well if the channel has an error type, why are we not using any error value?
	case _ = <-done:
		if !res.TimedOut {
			killProcessGroup(sc.pid)
		}
		log(verbose, "Execution (PID %d) completed at %s", sc.pid, time.Now().Format(TimeFormat))

		status := sc.cmd.ProcessState.Sys().(syscall.WaitStatus)

		if status.Signaled() {
			res.Signal = uint8(status.Signal())
		} else {
			res.Status = uint8(status.ExitStatus())
		}

	}

	res.Stdout = sc.out.Bytes()
	res.Stderr = sc.err.Bytes()

	return &res
}

func killProcessGroup(pid int) {
	syscall.Kill(-pid, syscall.SIGKILL)
}

func log(verbose bool, format string, args ...interface{}) {
	if verbose && os.Getenv("BUGS_QUIET") == "" {
		fmt.Fprintf(os.Stderr, format+"\n", args...)
	}
}

// Run executes the command described by its receiver and returns
// a report for the execution. If the execution cannot proceed, an error
// is returned, which is always a CmdError
func (spec CmdSpec) Run(verbose bool) (*CmdReport, error) {
	sc, err := spec.start(verbose)

	if err != nil {
		return nil, err
	}

	return spec.waitCmd(verbose, sc), nil
}
