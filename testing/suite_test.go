package testing

import (
	"errors"
	gotest "testing"

	. "github.com/smartystreets/goconvey/convey"

	"github.com/Epitech/bugs/cmd"
	"github.com/Epitech/bugs/cmd_aux"
)

func TestErrorInTestSuite(t *gotest.T) {

	Convey("A test suite with a spec-caused command error", t, func() {

		cerr := cmd.CmdError{true, errors.New("spec error")}

		tsuite := Suite{
			Name: "errorsuite",
			Tests: []testSpec{
				aTestData("Buggy").FakingError(cerr).Signal(),
			},
		}

		Convey("Should return a descriptive error when run", func() {
			cfgMock := cmd_aux.QuietMockRunnerConfig()

			err, rep := tsuite.Run(cfgMock)

			So(err.Error(), ShouldEqual,
				`Bugs or test spec error in suite errorsuite:
  In test Buggy: Invalid command: spec error`)

			So(rep, ShouldBeNil)

		})
	})

	Convey("A test suite with an unexpected spec error", t, func() {

		tsuite := Suite{
			Name: "errorsuite",
			Tests: []testSpec{
				aTestData("Buggy").FakingError(errors.New("oops")).Signal(),
			},
		}

		Convey("Should return a descriptive error when run", func() {
			cfgMock := cmd_aux.QuietMockRunnerConfig()

			err, rep := tsuite.Run(cfgMock)

			So(err.Error(), ShouldEqual,
				`Bugs or test spec error in suite errorsuite:
  In test Buggy: oops`)

			So(rep, ShouldBeNil)

		})

	})

	Convey("A test suite with a *nil* spec error", t, func() {

		tsuite := Suite{
			Name: "errorsuite",
			Tests: []testSpec{
				aTestData("Buggy").FakingError(nil).Signal(),
			},
		}

		Convey("Should return a descriptive error when run", func() {
			cfgMock := cmd_aux.QuietMockRunnerConfig()

			err, rep := tsuite.Run(cfgMock)

			So(err.Error(), ShouldEqual,
				`Bugs or test spec error in suite errorsuite:
  In test Buggy: [NO RESULT RETURNED]`)

			So(rep, ShouldBeNil)

		})

	})

}

func TestNewTestSuiteOrdersByName(t *gotest.T) {
	Convey("NewTestSuite", t, func() {

		skill := Skill{
			Diff: []DiffData{
				{TestData: TestData{Name: "a: diff#1"}},
				{TestData: TestData{Name: "c: diff#2"}},
				{TestData: TestData{Name: "b: diff#3"}},
			},
			Status: []StatusData{
				{TestData: TestData{Name: "a: status#1"}},
				{TestData: TestData{Name: "c: status#2"}},
			},
		}

		ts := NewTestSuite("skill", skill, TestOptions{})

		Convey("Orders a test suite's tests by name", func() {

			expected := []string{
				"a: diff#1",
				"a: status#1",
				"b: diff#3",
				"c: diff#2",
				"c: status#2",
			}

			actual := []string{}
			for _, t := range ts.Tests {
				actual = append(actual, t.TestName())
			}

			So(actual, ShouldResemble, expected)
		})

	})
}
