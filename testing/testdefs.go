package testing

import (
	"errors"
	"fmt"
	"io/ioutil"
	"regexp"
	"syscall"
	"time"

	"github.com/Epitech/bugs/cmd"
)

type (
	// TODO having un-validated, nullable values in the
	// final struct is baaaad, but that's how they are
	// unmarshalled and we would like not to have dupe
	// structs, so what to do?
	TestOptions struct {
		NoStatusCheck *bool   `json:",omitempty"`
		Timeout       *string `json:",omitempty"`
		SoftTimeout   *bool   `json:",omitempty"`
		RunAsStudent  *bool   `json:",omitempty"`
	}

	// TODO this is still a horrible c**********k
	Skill struct {
		TestOptions
		TraceTypes []string     `json:",omitempty"`
		Diff       []DiffData   `json:",omitempty"`
		Output     []OutputData `json:",omitempty"`
		Signal     []SignalData `json:",omitempty"`
		Status     []StatusData `json:",omitempty"`
	}

	TestData struct {
		TestOptions
		Name      string
		Mandatory bool
		Cmd       string
		Input     string
	}

	DiffData struct {
		TestData
		Expected string
		Output   string
	}

	OutputData struct {
		TestData
		Expected string
	}

	SignalData struct {
		TestData
	}

	StatusData struct {
		TestData
		Expected int `json:",omitempty"`
	}
)

func (sk Skill) TestNames() []string {
	names := make([]string, 0, len(sk.Diff)+len(sk.Output)+len(sk.Signal)+len(sk.Status))
	for _, t := range sk.Diff {
		names = append(names, t.Name)
	}
	for _, t := range sk.Output {
		names = append(names, t.Name)
	}
	for _, t := range sk.Signal {
		names = append(names, t.Name)
	}
	for _, t := range sk.Status {
		names = append(names, t.Name)
	}
	return names
}

func (td TestData) TestName() string {
	return td.Name
}

func (td TestData) TestMandatory() bool {
	return td.Mandatory
}

func (td TestData) CheckStatus() bool {
	return !td.IgnoreStatus()
}

func (td TestData) IgnoreStatus() bool {
	return td.NoStatusCheck != nil && *(td.NoStatusCheck)
}

func (td TestData) CommandTimeout() (timeout time.Duration) {
	if td.Timeout == nil {
		timeout = cmd.DefaultTimeout()
		return
	}
	timeout, terr := time.ParseDuration(*(td.Timeout))
	if terr != nil {
		timeout = cmd.DefaultTimeout()
	}

	return
}

func (td TestData) AllowTimeout() bool {
	return td.SoftTimeout != nil && *(td.SoftTimeout)
}

func (td TestData) UseStudentCredential() bool {
	return td.RunAsStudent != nil && *(td.RunAsStudent)
}

func (td TestData) getCredential(cfg cmd.RunnerConfig) *syscall.Credential {
	if td.UseStudentCredential() {
		return cfg.StudentCredential()
	}
	return cfg.TestRunnerCredential()
}

func (td TestData) command(cfg cmd.RunnerConfig) cmd.CmdSpec {
	var input []byte = []byte(td.Input)
	if len(input) == 0 {
		input = nil
	}

	return cmd.NewCmdLine(
		td.Cmd,
		td.getCredential(cfg),
		input,
		td.CommandTimeout(),
	)
}

func (td TestData) RunCommand(cfg cmd.RunnerConfig) (*cmd.CmdReport, error) {
	return td.command(cfg).Run(cfg.Verbose())
}

func (SignalData) CheckStatus() bool {
	return false
}

func (StatusData) CheckStatus() bool {
	return false
}

func (SignalData) Test(report cmd.CmdReport) (res testResult) {
	return
}

func (s StatusData) Test(report cmd.CmdReport) (res testResult) {
	if s.Expected != int(report.Status) {
		res.TestError = errors.New(fmt.Sprintf(
			"Expected exit status %d, got %d.", s.Expected, report.Status))
	}

	return
}

func (o OutputData) Test(report cmd.CmdReport) (res testResult) {
	match, err := regexp.Match(o.Expected, report.Stdout)

	if err != nil {
		res.SpecError = errors.New(fmt.Sprintf(
			"Regexp compilation failed for \"%s\": %s", o.Expected, err.Error()))
		return
	}

	if !match {
		res.TestError = errors.New(fmt.Sprintf("The output must match the regular expression '%s', but it was '%s'",
			o.Expected, report.Stdout))
	}

	return
}

func (d DiffData) Test(report cmd.CmdReport) (res testResult) {
	var err error
	var expectedBytes []byte
	var actualBytes []byte

	expectedBytes, err = ioutil.ReadFile(d.Expected)

	if err != nil {
		res.SpecError = errors.New(fmt.Sprintf(
			"Unable to read reference file %s", d.Expected))
		return
	}

	actualBytes, err = ioutil.ReadFile(d.Output)

	if err != nil {
		res.TestError = errors.New(fmt.Sprintf(
			"Unable to read output file %s", d.Output))
		return
	}

	if cmp, cr := compare(string(expectedBytes), string(actualBytes)); !cmp {
		res.TestError = errors.New(cr.String())
	}

	return
}
