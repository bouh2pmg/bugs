package testing

var (
	SignalsByName map[string]uint8 = map[string]uint8{
		"ABRT":   0x06,
		"ALRM":   0x0e,
		"BUS":    0x07,
		"CHLD":   0x11,
		"CLD":    0x11,
		"CONT":   0x12,
		"FPE":    0x08,
		"HUP":    0x01,
		"ILL":    0x04,
		"INT":    0x02,
		"IO":     0x1d,
		"IOT":    0x06,
		"KILL":   0x09,
		"PIPE":   0x0d,
		"POLL":   0x1d,
		"PROF":   0x1b,
		"PWR":    0x1e,
		"QUIT":   0x03,
		"SEGV":   0x0b,
		"STKFLT": 0x10,
		"STOP":   0x13,
		"SYS":    0x1f,
		"TERM":   0x0f,
		"TRAP":   0x05,
		"TSTP":   0x14,
		"TTIN":   0x15,
		"TTOU":   0x16,
		"UNUSED": 0x1f,
		"URG":    0x17,
		"USR1":   0x0a,
		"USR2":   0x0c,
		"VTALRM": 0x1a,
		"WINCH":  0x1c,
		"XCPU":   0x18,
		"XFSZ":   0x19,
	}

	SignalsByCode = byCode(SignalsByName)

	CrashSignals map[uint8]struct{} = map[uint8]struct{}{
		0x06: struct{}{},
		0x07: struct{}{},
		0x08: struct{}{},
		0x0b: struct{}{},
		0x0d: struct{}{},
	}
)

func byCode(byName map[string]uint8) map[uint8]string {
	var res map[uint8]string = make(map[uint8]string)
	for n, c := range byName {
		res[c] = n
	}
	return res
}

func DescribeSignal(signal uint8) string {
	name, ok := SignalsByCode[signal]
	if !ok {
		return "[UNKNOWN]"
	}
	return "SIG" + name
}

func IsCrashSignal(signal uint8) bool {
	_, ok := CrashSignals[signal]
	return ok
}
