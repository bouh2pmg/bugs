package testing

import (
	"reflect"
	"testing"
)

func TestCompareOK(t *testing.T) {
	str := "some string"
	expectedCompare := CompareResult{}
	gotRes, gotCompare := compare(str, str)

	if !gotRes {
		t.Errorf("Strings should be equal")
	}
	if !reflect.DeepEqual(gotCompare, expectedCompare) {
		t.Errorf("Got %q but expected %q", gotCompare, expectedCompare)
	}
	if len(gotCompare.String()) > 0 {
		t.Errorf("Should format to an empty string")
	}
}

func TestCompareKO(t *testing.T) {
	str1 := "first string"
	str2 := "second string"
	expectedCompare := CompareResult{LineNum: 1, Left: str1, Right: str2}
	gotRes, gotCompare := compare(str1, str2)

	if gotRes {
		t.Errorf("Strings should not be equal")
	}
	if !reflect.DeepEqual(gotCompare, expectedCompare) {
		t.Errorf("Got %q but expected %q", gotCompare, expectedCompare)
	}
	if len(gotCompare.String()) == 0 {
		t.Errorf("Should format to a useful string")
	}
}

func expectFileCompareError(
	t *testing.T,
	gotRes bool,
	gotCompare CompareResult,
	compareErr error,
	expectedCompare CompareResult) {

	if compareErr != nil {
		t.Errorf("Files should be valid")
	}
	if gotRes {
		t.Errorf("Files content should not be equal")
	}
	if !reflect.DeepEqual(gotCompare, expectedCompare) {
		t.Errorf("Got %q but expected %q", gotCompare, expectedCompare)
	}
	if len(gotCompare.String()) == 0 {
		t.Errorf("Should format to a useful string")
	}
}

func TestCompareFile(t *testing.T) {
	expectedCompare := CompareResult{
		LineNum: 22,
		Left:    "elementum et vestibulum eu, porttitor vel elit. Curabitur venenatis pulvinar",
		Right:   "et, tincidunt et orci. Fusce eget orci a orci congue vestibulum. Ut dolor diam,",
	}
	gotRes, gotCompare, err := compareFiles("_test/lorem", "_test/ipsum")

	expectFileCompareError(t, gotRes, gotCompare, err, expectedCompare)
}

func TestCompareFileInitial(t *testing.T) {
	expectedCompare := CompareResult{
		LineNum: 3,
		Left:    "<no line>",
		Right:   "Indeed, here is an extra line",
	}

	gotRes, gotCompare, err := compareFiles("_test/init", "_test/full")

	expectFileCompareError(t, gotRes, gotCompare, err, expectedCompare)
}

func TestCompareFileFinal(t *testing.T) {
	expectedCompare := CompareResult{
		LineNum: 3,
		Left:    "Indeed, here is an extra line",
		Right:   "<no line>",
	}

	gotRes, gotCompare, err := compareFiles("_test/full", "_test/init")

	expectFileCompareError(t, gotRes, gotCompare, err, expectedCompare)
}

func TestCompareNoFile(t *testing.T) {
	data := []struct {
		l string
		r string
	}{
		{"nope", "nope"},
		{"_test/lorem", "nope"},
	}
	for _, it := range data {
		if _, _, err := compareFiles(it.l, it.r); err == nil {
			t.Errorf("It should raise an error")
		}
	}
}
