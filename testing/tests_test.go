package testing

import (
	gotest "testing"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/Epitech/bugs/cmd"
)

var (
	testAnalysisExpectations = []TestResultExpectation{
		{
			"Signal test",
			aTestData("TEST").Signal(),
			"command exiting normally",
			cmd.CmdReport{make([]byte, 0), make([]byte, 0), 0, 0, false},
			"pass",
			shouldPass,
		},
		{
			"Signal test",
			aTestData("TEST").Signal(),
			"command raising a non-crashing signal",
			cmd.CmdReport{make([]byte, 0), make([]byte, 0), 0, 1, false},
			"pass",
			shouldPass,
		},
		{
			"Signal test",
			aTestData("TEST").Signal(),
			"command raising a crashing signal",
			cmd.CmdReport{make([]byte, 0), make([]byte, 0), 0, 11, false},
			"crash",
			shouldBeACrash,
		},
		{
			"Signal test",
			aTestData("TEST").Signal(),
			"command raising a crashing signal",
			cmd.CmdReport{make([]byte, 0), make([]byte, 0), 0, 11, false},
			"crash",
			shouldHaveReason("Crashed - signal SIGSEGV"),
		},
		{
			"Signal test",
			aTestData("TEST").Signal(),
			"command exiting with status 1",
			cmd.CmdReport{make([]byte, 0), make([]byte, 0), 1, 0, false},
			"pass",
			shouldPass,
		},
		{
			"Status test expecting 0",
			aTestData("TEST").Status(0),
			"command exiting with status 1",
			cmd.CmdReport{make([]byte, 0), make([]byte, 0), 1, 0, false},
			"not pass",
			shouldNotPass,
		},
		{
			"Status test expecting 0",
			aTestData("TEST").Status(0),
			"command exiting with status 1",
			cmd.CmdReport{make([]byte, 0), make([]byte, 0), 1, 0, false},
			"not pass",
			shouldHaveReason("Test failure: Expected exit status 0, got 1."),
		},
		{
			"Status test expecting 0",
			aTestData("TEST").Status(0),
			"command exiting with status 139",
			cmd.CmdReport{make([]byte, 0), make([]byte, 0), 139, 0, false},
			"crash",
			shouldBeACrash,
		},
		{
			"Status test expecting 0",
			aTestData("TEST").Status(0),
			"command exiting with status 139",
			cmd.CmdReport{make([]byte, 0), make([]byte, 0), 139, 0, false},
			"crash",
			shouldHaveReason("Crashed - exit status 139"),
		},
		{
			"Output test",
			aTestData("TEST").Output("ola"),
			"command with the expected output and status 0",
			cmd.CmdReport{[]byte("ola"), make([]byte, 0), 0, 0, false},
			"pass",
			shouldPass,
		},
		{
			"Output test",
			aTestData("TEST").Output("ola"),
			"command with the expected output and status 1",
			cmd.CmdReport{[]byte("ola"), make([]byte, 0), 1, 0, false},
			"not pass",
			shouldNotPass,
		},
		{
			"Output test",
			aTestData("TEST").Output("ola"),
			"command with the expected output and status 1",
			cmd.CmdReport{[]byte("ola"), make([]byte, 0), 1, 0, false},
			"not pass",
			shouldHaveReason("Invalid exit status 1"),
		},
		{
			"Output test without status check",
			aTestData("TEST").DontCheckStatus().Output("ola"),
			"command with the expected output and status 1",
			cmd.CmdReport{[]byte("ola"), make([]byte, 0), 1, 0, false},
			"pass",
			shouldPass,
		},
		{
			"Output test",
			aTestData("TEST").Output(""),
			"command exiting with status 1",
			cmd.CmdReport{make([]byte, 0), make([]byte, 0), 1, 0, false},
			"not crash",
			shouldNotBeACrash,
		},
		{
			"Output test",
			aTestData("TEST").Output(""),
			"command exiting with status 139",
			cmd.CmdReport{make([]byte, 0), make([]byte, 0), 139, 0, false},
			"crash",
			shouldBeACrash,
		},
		{
			"Output test",
			aTestData("TEST").Output(""),
			"command raising signal 11",
			cmd.CmdReport{make([]byte, 0), make([]byte, 0), 255, 11, false},
			"crash",
			shouldBeACrash,
		},
		{
			"Output test",
			aTestData("TEST").Output(""),
			"command that times out",
			cmd.CmdReport{[]byte("ola"), make([]byte, 0), 0, 0, true},
			"crash",
			shouldBeACrash,
		},
		{
			"Output test",
			aTestData("TEST").Output(""),
			"command that times out",
			cmd.CmdReport{[]byte("ola"), make([]byte, 0), 0, 0, true},
			"crash",
			shouldHaveReason("Crashed - Timed out after " + cmd.DefaultTimeout().String()),
		},
		{
			"Output test with soft timeout",
			aTestData("TEST").WithSoftTimeout().Output(""),
			"command that times out (with the correct output)",
			cmd.CmdReport{[]byte("ola"), make([]byte, 0), 0, 0, true},
			"not pass",
			shouldNotPass,
		},
		{
			"Output test with soft timeout",
			aTestData("TEST").WithSoftTimeout().Output(""),
			"command that times out (with the correct output)",
			cmd.CmdReport{[]byte("ola"), make([]byte, 0), 0, 0, true},
			"not pass",
			shouldHaveReason("Timed out after " + cmd.DefaultTimeout().String()),
		},
	}
)

func TestAnalyseTestResult(t *gotest.T) {
	for _, s := range testAnalysisExpectations {
		Convey("A(n) "+s.testSpecDescr, t, func() {

			var t testSpec = s.testSpec
			Convey("Analysing a(n) "+s.cmdReportDescr, func() {
				var r cmd.CmdReport = s.cmdReport
				report, _ := analyzeTestRun(t, &r)

				Convey("Should "+s.assertionDescr, func() {
					So(*report, s.assertion)
				})
			})

		})
	}
}

func TestAnalyzeTestResultWithSpecError(t *gotest.T) {
	Convey("An output test with an invalid regex", t, func() {
		td := aTestData("TEST").Output("ab.(d")
		Convey("Analysing a command exiting normally", func() {
			rep := cmd.CmdReport{make([]byte, 0), make([]byte, 0), 0, 0, false}

			Convey("Should return an error", func() {
				_, err := analyzeTestRun(td, &rep)

				So(err.Error(), ShouldStartWith, `Regexp compilation failed for "ab.(d"`)
			})
		})
	})
}
