package testing

import (
	"fmt"
	"os"
	"sort"
	"strings"

	"github.com/Epitech/bugs/cmd"
	"github.com/Epitech/bugs/results"
)

type (
	Suite struct {
		Name            string
		TraceTypes      []string
		Tests           []testSpec
		Crashed         bool
		ForbidPitch     bool
		MandatoryFailed bool
	}

	testSpecs []testSpec
)

func (ts testSpecs) Len() int      { return len(ts) }
func (ts testSpecs) Swap(i, j int) { ts[i], ts[j] = ts[j], ts[i] }
func (ts testSpecs) Less(i, j int) bool {
	return ts[i].TestName() < ts[j].TestName()
}

func setTestOptions(test *TestOptions, options TestOptions) {
	if test.NoStatusCheck == nil {
		test.NoStatusCheck = options.NoStatusCheck
	}
	if test.Timeout == nil {
		test.Timeout = options.Timeout
	}
	if test.SoftTimeout == nil {
		test.SoftTimeout = options.SoftTimeout
	}
	if test.RunAsStudent == nil {
		test.RunAsStudent = options.RunAsStudent
	}
}

func NewTestSuite(skillName string, skill Skill, options TestOptions) *Suite {
	setTestOptions(&(skill.TestOptions), options)

	tests := []testSpec{}

	for _, t := range skill.Diff {
		setTestOptions(&(t.TestOptions), skill.TestOptions)
		tests = append(tests, t)
	}
	for _, t := range skill.Output {
		setTestOptions(&(t.TestOptions), skill.TestOptions)
		tests = append(tests, t)
	}
	for _, t := range skill.Signal {
		setTestOptions(&(t.TestOptions), skill.TestOptions)
		tests = append(tests, t)
	}
	for _, t := range skill.Status {
		setTestOptions(&(t.TestOptions), skill.TestOptions)
		tests = append(tests, t)
	}

	sort.Sort(testSpecs(tests))

	return &Suite{
		Name:       skillName,
		TraceTypes: skill.TraceTypes,
		Tests:      tests,
	}
}

// Run every test in the suite.
func (ts *Suite) Run(cfg cmd.RunnerConfig) (error, *results.TestRunSkill) {
	var specErrors []string

	testRunItems := []*results.TestRunItem{}

	fmt.Fprintf(os.Stderr, "==> Test suite: %s\n", ts.Name)
	for _, t := range ts.Tests {
		fmt.Fprintf(os.Stderr, "====> Test: %s\n", t.TestName())
		cmdReport, item, error := runTest(cfg, t)
		if error != nil {
			specErrors = append(specErrors,
				fmt.Sprintf("  In test %s: %s", t.TestName(), error.Error()))
			break
		}
		if item == nil {
			specErrors = append(specErrors,
				fmt.Sprintf("  In test %s: [NO RESULT RETURNED]", t.TestName()))
			break
		}

		// TODO require flag for the program output part?
		if cmdReport != nil {
			fmt.Fprintln(os.Stderr, "----- PROGRAM OUTPUT (stdout) -----")
			fmt.Fprint(os.Stderr, string(cmdReport.Stdout))
			fmt.Fprintln(os.Stderr, "----- PROGRAM OUTPUT (stderr) -----")
			fmt.Fprint(os.Stderr, string(cmdReport.Stderr))
			fmt.Fprintln(os.Stderr, "------ END OF PROGRAM OUTPUT ------")
			fmt.Fprintln(os.Stderr, "----- END OF PROGRAM EXECUTION ----")
		}

		fmt.Fprintln(os.Stderr, *item.Comment)
		if item.Crashed {
			fmt.Fprintln(os.Stderr, "======> Test crashed")
		}
		if t.TestMandatory() && !item.Passed {
			fmt.Fprintln(os.Stderr, "======> Mandatory test failed")
		}
		testRunItems = append(testRunItems, item)
	}

	if len(specErrors) > 0 {
		err := fmt.Errorf(
			"Bugs or test spec error in suite %s:\n%s",
			ts.Name,
			strings.Join(specErrors, "\n"),
		)
		return err, nil
	}
	return nil, &results.TestRunSkill{ts.Name, ts.TraceTypes, testRunItems}
}
