package testing

import (
	"time"

	"github.com/Epitech/bugs/cmd"
)

type (
	TestDataBuilder struct {
		// TestData
		name          string
		cmd           string
		mandatory     bool
		input         *string
		noStatusCheck *bool
		timeout       *string
		softTimeout   *bool
		runAsStudent  *bool
	}

	testFake struct {
		delegate  testSpec
		cmdReport *cmd.CmdReport
		specError error
	}

	fakeBuilder struct {
		TestDataBuilder
		report    *cmd.CmdReport
		specError error
	}
)

func aTestData(name string) TestDataBuilder {
	return TestDataBuilder{name: name, cmd: ""}
}

func (b TestDataBuilder) WithCmd(cmd string) TestDataBuilder {
	b.cmd = cmd
	return b
}

func (b TestDataBuilder) Mandatory() TestDataBuilder {
	b.mandatory = true
	return b
}

func (b TestDataBuilder) WithInput(input string) TestDataBuilder {
	b.input = &input
	return b
}

func (b TestDataBuilder) DontCheckStatus() TestDataBuilder {
	v := true
	b.noStatusCheck = &v
	return b
}

func (b TestDataBuilder) DoCheckStatus() TestDataBuilder {
	v := false
	b.noStatusCheck = &v
	return b
}

func (b TestDataBuilder) WithTimeout(timeout string) TestDataBuilder {
	b.timeout = &timeout
	return b
}

func (b TestDataBuilder) WithSoftTimeout() TestDataBuilder {
	v := true
	b.softTimeout = &v
	return b
}

func (b TestDataBuilder) WithHardTimeout() TestDataBuilder {
	v := false
	b.softTimeout = &v
	return b
}

func (b TestDataBuilder) RunAsStudent() TestDataBuilder {
	v := true
	b.runAsStudent = &v
	return b
}

func (b TestDataBuilder) RunAsTestRunner() TestDataBuilder {
	v := false
	b.runAsStudent = &v
	return b
}

func (b TestDataBuilder) toTestData() TestData {

	emptyOr := func(s *string) string {
		if s == nil {
			return ""
		}

		return *s
	}

	return TestData{
		TestOptions{
			b.noStatusCheck,
			b.timeout,
			b.softTimeout,
			b.runAsStudent,
		},
		b.name,
		b.mandatory,
		b.cmd,
		emptyOr(b.input),
	}
}

func (b TestDataBuilder) Signal() (x SignalData) {
	x = SignalData{TestData: b.toTestData()}
	return
}

func (b TestDataBuilder) Status(expected int) (x StatusData) {
	x = StatusData{TestData: b.toTestData(), Expected: expected}
	return
}

func (b TestDataBuilder) Output(expected string) (x OutputData) {
	x = OutputData{TestData: b.toTestData(), Expected: expected}
	return
}

func (b TestDataBuilder) Diff(expected string, outputFile string) (x DiffData) {
	x = DiffData{TestData: b.toTestData(), Expected: expected, Output: outputFile}
	return
}

func (t testFake) TestName() string {
	return t.delegate.TestName()
}

func (t testFake) TestMandatory() bool {
	return t.delegate.TestMandatory()
}

func (t testFake) RunCommand(cfg cmd.RunnerConfig) (*cmd.CmdReport, error) {
	return t.cmdReport, t.specError
}

func (t testFake) IgnoreStatus() bool {
	return !t.CheckStatus()
}

func (t testFake) CheckStatus() bool {
	return t.delegate.CheckStatus()
}

func (t testFake) CommandTimeout() time.Duration {
	return t.delegate.CommandTimeout()
}

func (t testFake) AllowTimeout() bool {
	return t.delegate.AllowTimeout()
}

func (t testFake) UseStudentCredential() bool {
	return t.delegate.UseStudentCredential()
}

func (t testFake) Test(cmdReport cmd.CmdReport) testResult {
	return t.delegate.Test(cmdReport)
}

func (b TestDataBuilder) Faking(cmdReport cmd.CmdReport) fakeBuilder {
	return fakeBuilder{
		b,
		&cmdReport,
		nil,
	}
}

func (b TestDataBuilder) FakingError(err error) fakeBuilder {
	return fakeBuilder{
		b,
		nil,
		err,
	}
}

func (b fakeBuilder) Signal() testFake {
	return testFake{b.TestDataBuilder.Signal(), b.report, b.specError}
}

func (b fakeBuilder) Status(expected int) testFake {
	return testFake{b.TestDataBuilder.Status(expected), b.report, b.specError}
}

func (b fakeBuilder) Output(expected string) testFake {
	return testFake{b.TestDataBuilder.Output(expected), b.report, b.specError}
}

func (b fakeBuilder) Diff(expected string, outputFile string) testFake {
	return testFake{b.TestDataBuilder.Diff(expected, outputFile), b.report, b.specError}
}
