package testing

import (
	"fmt"

	"github.com/Epitech/bugs/results"
)

var (
	testResultFmt string = `Expected a %s error with error message <%s>, got %+v.`

	testReportFmt string = `Expected a TestReport with %s, got %+v.`

	shouldBeACrash = assertCrash(true)

	shouldNotBeACrash = assertCrash(false)

	shouldPass = assertPass(true)

	shouldNotPass = assertPass(false)
)

// TestReport asserts

func assertTestReport(failureMessage func(interface{}) string,
	predicate func(report results.TestRunItem) bool) func(interface{}, ...interface{}) string {
	return func(actual interface{}, expected ...interface{}) string {
		testReport, ok := actual.(results.TestRunItem)
		if !ok {
			return failureMessage(actual)
		}

		if p := predicate(testReport); !p {
			return failureMessage(actual)
		}

		return ""
	}
}

func assertCrash(shouldCrash bool) func(interface{}, ...interface{}) string {
	failed := func(actual interface{}) string {
		return fmt.Sprintf(testReportFmt,
			fmt.Sprintf("{Crashed: %t, Passed: false}", shouldCrash), actual)
	}

	return assertTestReport(failed, func(result results.TestRunItem) bool {
		return result.Crashed == shouldCrash && !result.Passed
	})
}

func assertPass(shouldPass bool) func(interface{}, ...interface{}) string {
	failed := func(actual interface{}) string {
		return fmt.Sprintf(testReportFmt,
			fmt.Sprintf("{Crashed: false, Passed: %t}", shouldPass), actual)
	}

	return assertTestReport(failed, func(result results.TestRunItem) bool {
		return result.Passed == shouldPass && !result.Crashed
	})
}

func shouldHaveReason(expected string) func(interface{}, ...interface{}) string {
	failed := func(actual interface{}) string {
		return fmt.Sprintf(testReportFmt,
			fmt.Sprintf("{Reason: %s}", expected), actual)
	}

	return assertTestReport(failed, func(result results.TestRunItem) bool {
		return *result.Comment == expected
	})
}

// testResult asserts

func (t testResult) String() string {
	nilOrErrorString := func(err error) string {
		if err == nil {
			return "<nil>"
		}
		return "\"" + err.Error() + "\""
	}
	return fmt.Sprintf("{ TestError: %s, SpecError: %s }",
		nilOrErrorString(t.TestError),
		nilOrErrorString(t.SpecError))
}

func shouldBeATestError(expectedError string) (f func(interface{}, ...interface{}) string) {

	f = func(actual interface{}, expected ...interface{}) string {
		failed := func() string {
			return fmt.Sprintf(testResultFmt, "test", expectedError, actual)
		}

		testResult, ok := actual.(testResult)
		if !ok {
			return failed()
		}

		if testResult.TestError == nil {
			return failed()
		}

		if testResult.TestError.Error() != expectedError {
			return failed()
		}

		return ""
	}

	return
}

func shouldBeASpecError(expectedError string) (f func(interface{}, ...interface{}) string) {

	f = func(actual interface{}, expected ...interface{}) string {
		failed := func() string {
			return fmt.Sprintf(testResultFmt, "spec", expectedError, actual)
		}

		testResult, ok := actual.(testResult)
		if !ok {
			return failed()
		}

		if testResult.SpecError == nil {
			return failed()
		}

		if testResult.SpecError.Error() != expectedError {
			return failed()
		}

		return ""

	}

	return
}

func shouldBeAnEmptyResult(actual interface{}, expected ...interface{}) string {
	failed := func() string {
		descr := "Expected a PASS result"
		return fmt.Sprintf("%s, got <%s>.", descr, actual)
	}

	testResult, ok := actual.(testResult)
	if !ok {
		return failed()
	}

	if testResult.TestError != nil || testResult.SpecError != nil {
		return failed()
	}

	return ""
}

func safeIndexString(slice []interface{}, index int) (string, bool) {
	if index < len(slice) {
		res, ok := slice[index].(string)
		if ok {
			return res, true
		}
	}
	return "", false
}
