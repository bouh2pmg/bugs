package testing

import (
	"fmt"
	"io/ioutil"
	"strings"
)

type CompareResult struct {
	LineNum int
	Left    string
	Right   string
}

func (cr CompareResult) String() (str string) {
	if cr.LineNum == 0 {
		return ""
	}
	return fmt.Sprintf("Line %d differs\n< %s\n> %s",
		cr.LineNum, cr.Left, cr.Right)
}

func compare(left, right string) (bool, CompareResult) {
	llines := strings.Split(left, "\n")
	rlines := strings.Split(right, "\n")
	for i := 0; i < len(llines) || i < len(rlines); i++ {
		if i >= len(llines) {
			return false, CompareResult{
				LineNum: i + 1,
				Left:    "<no line>",
				Right:   rlines[i],
			}
		}
		if i >= len(rlines) {
			return false, CompareResult{
				LineNum: i + 1,
				Left:    llines[i],
				Right:   "<no line>",
			}
		}
		if llines[i] != rlines[i] {
			return false, CompareResult{
				LineNum: i + 1,
				Left:    llines[i],
				Right:   rlines[i],
			}
		}
	}
	return true, CompareResult{}
}

func compareFiles(left, right string) (res bool, cr CompareResult, err error) {
	lraw, err := ioutil.ReadFile(left)
	if err != nil {
		return
	}
	rraw, err := ioutil.ReadFile(right)
	if err != nil {
		return
	}
	res, cr = compare(string(lraw), string(rraw))
	return
}
