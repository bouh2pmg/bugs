package testing

import (
	"syscall"
	gotest "testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"

	"github.com/Epitech/bugs/cmd"
	"github.com/Epitech/bugs/cmd_aux"
)

type (
	TestResultExpectation struct {
		testSpecDescr  string
		testSpec       testSpec
		cmdReportDescr string
		cmdReport      cmd.CmdReport
		assertionDescr string
		assertion      func(interface{}, ...interface{}) string
	}
)

var expectedTestResults = []TestResultExpectation{
	{
		"'Signal' test",
		aTestData("signal").Signal(),
		"non-signalling command",
		cmd.CmdReport{make([]byte, 0), make([]byte, 0), 0, 0, false},
		"pass",
		shouldBeAnEmptyResult,
	},
	{
		"'Signal' test",
		aTestData("signal").Signal(),
		"signalling command",
		cmd.CmdReport{make([]byte, 0), make([]byte, 0), 137, 9, true},
		"pass",
		shouldBeAnEmptyResult,
	},
	{
		"'Status' test expecting 0",
		aTestData("status").Status(0),
		"command returning 0",
		cmd.CmdReport{make([]byte, 0), make([]byte, 0), 0, 0, false},
		"pass",
		shouldBeAnEmptyResult,
	},
	{
		"'Status' test expecting 1",
		aTestData("status").Status(1),
		"command returning 0",
		cmd.CmdReport{make([]byte, 0), make([]byte, 0), 0, 0, false},
		"yield a test error",
		shouldBeATestError("Expected exit status 1, got 0."),
	},
	{
		"'Status' test expecting 0",
		aTestData("status").Status(0),
		"command returning 1",
		cmd.CmdReport{make([]byte, 0), make([]byte, 0), 1, 0, false},
		"yield a test error",
		shouldBeATestError("Expected exit status 0, got 1."),
	},
	{
		"'Status' test expecting 1",
		aTestData("status").Status(1),
		"command returning 1",
		cmd.CmdReport{make([]byte, 0), make([]byte, 0), 1, 0, false},
		"pass",
		shouldBeAnEmptyResult,
	},
	{
		"'Output' test",
		aTestData("output").Output("ab.d"),
		"command printing the expected output",
		cmd.CmdReport{[]byte("abcd"), make([]byte, 0), 0, 0, false},
		"pass",
		shouldBeAnEmptyResult,
	},
	{
		"'Output' test",
		aTestData("output").Output("ab.d"),
		"command not printing the expected output",
		cmd.CmdReport{[]byte("abce"), make([]byte, 0), 0, 0, false},
		"yield a test error",
		shouldBeATestError("The output must match the regular expression 'ab.d', but it was 'abce'"),
	},
	{
		"'Output' test with invalid regexp",
		aTestData("output").Output("ab.(d"),
		"command",
		cmd.CmdReport{[]byte("abcd"), make([]byte, 0), 0, 0, false},
		"yield a spec error",
		shouldBeASpecError("Regexp compilation failed for \"ab.(d\": " +
			"error parsing regexp: missing closing ): `ab.(d`"),
	},
	{
		"'Diff' test of identical files",
		aTestData("diff").Diff("_test/lorem", "_test/lorem"),
		"command",
		cmd.CmdReport{make([]byte, 0), make([]byte, 0), 0, 0, false},
		"pass",
		shouldBeAnEmptyResult,
	},
	{
		"'Diff' test of different files",
		aTestData("diff").Diff("_test/lorem", "_test/ipsum"),
		"command",
		cmd.CmdReport{make([]byte, 0), make([]byte, 0), 0, 0, false},
		"yield a test error",
		shouldBeATestError(`Line 22 differs
< elementum et vestibulum eu, porttitor vel elit. Curabitur venenatis pulvinar
> et, tincidunt et orci. Fusce eget orci a orci congue vestibulum. Ut dolor diam,`),
	},
	{
		"'Diff' test with missing expected file",
		aTestData("diff").Diff("_test/missing", "_test/lorem"),
		"command",
		cmd.CmdReport{make([]byte, 0), make([]byte, 0), 0, 0, false},
		"yield a spec error",
		shouldBeASpecError("Unable to read reference file _test/missing"),
	},
	{
		"'Diff' test with missing target file",
		aTestData("diff").Diff("_test/lorem", "_test/missing"),
		"command",
		cmd.CmdReport{make([]byte, 0), make([]byte, 0), 0, 0, false},
		"yield a test error",
		shouldBeATestError("Unable to read output file _test/missing"),
	},
}

func TestTestDefs(t *gotest.T) {
	for _, s := range expectedTestResults {
		Convey("A(n) "+s.testSpecDescr, t, func() {

			var t testSpec = s.testSpec
			Convey("Given a(n) "+s.cmdReportDescr, func() {
				var r cmd.CmdReport = s.cmdReport

				Convey("Should "+s.assertionDescr, func() {
					So(t.Test(r), s.assertion)
				})
			})

		})
	}
}

func TestTestDataCommand(t *gotest.T) {
	testRunnerCredential := &syscall.Credential{Uid: 1000}
	studentCredential := &syscall.Credential{Uid: 1001}

	Convey("When constructing the command", t, func() {

		cfgMock := cmd_aux.MakeRunnerConfig(false, testRunnerCredential, studentCredential)

		toCmdSpec := func(c string) cmd.CmdSpec {
			return aTestData("TEST").WithCmd(c).Signal().command(cfgMock)
		}

		Convey("The TestData 'cmd' parameter", func() {
			Convey("Is split for execution", func() {
				cmdSpec := toCmdSpec("cat foo.txt")
				So(cmdSpec.CmdParts, ShouldResemble, []string{"cat", "foo.txt"})
			})

			Convey("Double quotes are acknowledged", func() {
				cmdSpec := toCmdSpec(`echo "hello world"`)
				So(cmdSpec.CmdParts, ShouldResemble, []string{"echo", `hello world`})
			})
		})

		Convey("The TestData timeout parameter", func() {
			Convey("Is set on the command", func() {
				td := aTestData("TEST").WithCmd("whoami").WithTimeout("30s").Signal()

				cmdSpec := td.command(cfgMock)

				So(cmdSpec.Timeout, ShouldEqual, time.Duration(30)*time.Second)
			})

			Convey("Is replaced with default timeout when empty", func() {
				td := aTestData("TEST").WithCmd("whoami").WithTimeout("").Signal()

				cmdSpec := td.command(cfgMock)

				So(cmdSpec.Timeout, ShouldEqual, cmd.DefaultTimeout())
			})

			Convey("Is replaced with default timeout when invalid", func() {
				td := aTestData("TEST").WithCmd("whoami").WithTimeout("abcd").Signal()

				cmdSpec := td.command(cfgMock)

				So(cmdSpec.Timeout, ShouldEqual, cmd.DefaultTimeout())
			})

		})

		Convey("The TestData input parameter", func() {

			Convey("Is set on the command", func() {
				td := aTestData("TEST").WithCmd("cat").WithInput("hello").Signal()
				cmdSpec := td.command(cfgMock)

				So(cmdSpec.Input, ShouldResemble, []byte("hello"))
			})

		})

		Convey("When the TestData has RunAsStudent false", func() {
			Convey("The command is setup using the test runner credential", func() {

				td := aTestData("TEST").WithCmd("cat").RunAsTestRunner().Status(0)

				cmdSpec := td.command(cfgMock)

				So(cfgMock.CallsRecord, ShouldResemble, []uint{cmd_aux.AsTestRunner})
				So(cmdSpec.Credential, ShouldEqual, testRunnerCredential)
			})
		})

		Convey("When the TestData has RunAsStudent true", func() {
			Convey("The command is setup using the student credential", func() {

				td := aTestData("TEST").WithCmd("cat").RunAsStudent().Status(0)

				cmdSpec := td.command(cfgMock)

				So(cfgMock.CallsRecord, ShouldResemble, []uint{cmd_aux.AsStudent})
				So(cmdSpec.Credential, ShouldEqual, studentCredential)
			})
		})

		Convey("When the TestData has RunAsStudent nil", func() {
			Convey("The command is setup using the test runner credential", func() {

				td := aTestData("TEST").WithCmd("cat").Status(0)

				cmdSpec := td.command(cfgMock)

				So(cfgMock.CallsRecord, ShouldResemble, []uint{cmd_aux.AsTestRunner})
				So(cmdSpec.Credential, ShouldEqual, testRunnerCredential)
			})
		})
	})

}
