package testing

import (
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	os.MkdirAll("_test/.tmp", 0755)
	ret := m.Run()

	os.RemoveAll("_test/.tmp")
	os.Exit(ret)
}
