package testing

import (
	"fmt"
	"log"
	"time"

	"github.com/Epitech/bugs/cmd"
	"github.com/Epitech/bugs/results"
)

const (
	PassReason string = "PASSED"
)

type (
	testSpec interface {
		TestName() string
		TestMandatory() bool
		RunCommand(cfg cmd.RunnerConfig) (*cmd.CmdReport, error)
		CheckStatus() bool
		CommandTimeout() time.Duration
		AllowTimeout() bool
		IgnoreStatus() bool
		UseStudentCredential() bool
		Test(cmd.CmdReport) testResult
	}

	testResult struct {
		// Error in test specification
		SpecError error
		// Error in test execution
		TestError error
	}

	TestReport struct {
		Name      string
		Mandatory bool
		Crashed   bool
		Passed    bool
		Reason    string
		// TODO the CmdReport field is actually never used ?
		CmdReport *cmd.CmdReport
	}
)

func runTest(cfg cmd.RunnerConfig, t testSpec) (*cmd.CmdReport, *results.TestRunItem, error) {
	cmdReport, err := t.RunCommand(cfg)

	if cmdReport == nil {
		if cmdErr, ok := err.(cmd.CmdError); ok {
			item, err := recoverCommandError(t, cmdErr)
			return cmdReport, item, err
		}
		return cmdReport, nil, err
	}

	item, err := analyzeTestRun(t, cmdReport)

	return cmdReport, item, err
}

func recoverCommandError(t testSpec, err cmd.CmdError) (*results.TestRunItem, error) {

	if err.IsClientError {
		return nil, err
	}

	comment := fmt.Sprintf("Execution failed: " + err.Error())

	return &results.TestRunItem{
		Name:      t.TestName(),
		Mandatory: t.TestMandatory(),
		Crashed:   true,
		Passed:    false,
		Comment:   &comment,
	}, nil
}

func analyzeTestRun(t testSpec, cmdReport *cmd.CmdReport) (*results.TestRunItem, error) {

	testRunItem := &results.TestRunItem{
		Name:      t.TestName(),
		Mandatory: t.TestMandatory(),
	}

	var comment string

	if cmdReport.TimedOut {
		testRunItem.Crashed = !t.AllowTimeout()
		comment = fmt.Sprintf("Timed out after %s", t.CommandTimeout())
		if !t.AllowTimeout() {
			comment = "Crashed - " + comment
		}
		testRunItem.Comment = &comment
	} else if c, r := crashWithReason(cmdReport); c {
		testRunItem.Crashed = true
		comment = r
	} else if cmdReport.Status != 0 && t.CheckStatus() {
		comment = fmt.Sprintf("Invalid exit status %d", cmdReport.Status)
	} else {
		testResult := t.Test(*cmdReport)
		if testResult.SpecError != nil {
			log.Printf("SPEC ERROR: %s\n", testResult.SpecError.Error())
			return nil, testResult.SpecError
		}
		if testResult.TestError != nil {
			testRunItem.Passed = false
			comment = fmt.Sprintf("Test failure: %s", testResult.TestError.Error())
			testRunItem.Comment = &comment
		} else {
			testRunItem.Passed = true
			comment = PassReason
		}
	}

	testRunItem.Comment = &comment

	return testRunItem, nil
}

func crashWithReason(r *cmd.CmdReport) (crashed bool, reason string) {
	if IsCrashSignal(r.Signal) {
		crashed = true
		reason = fmt.Sprintf("Crashed - signal %s", DescribeSignal(r.Signal))
	} else if IsCrashSignal(r.Status - 128) {
		crashed = true
		reason = fmt.Sprintf("Crashed - exit status %d", r.Status)
	}

	return
}
