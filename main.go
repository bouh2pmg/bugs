package main

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/Epitech/bugs/cmd"
	"github.com/Epitech/bugs/data"
	"github.com/Epitech/bugs/options"
	"github.com/Epitech/bugs/project"
	"github.com/Epitech/bugs/report"
	"github.com/Epitech/bugs/results"
)

var (
	dbConfig *data.ApiConfig
)

func main() {
	options.BugsFlags.Parse(os.Args[1:])

	dbConfig = data.LoadDbConfig()

	if *options.Help {
		fmt.Fprintf(os.Stderr, "bugs version %s\n", Version)
		options.BugsFlags.PrintDefaults()
		return
	}

	if *options.Ver {
		fmt.Fprintf(os.Stderr, "bugs version %s\n", Version)
		return
	}

	if *options.Cfg {
		fmt.Fprintln(os.Stderr,
			"# Default config (./bugs.cfg or $HOME/.bugs/bugs.cfg or /etc/bugs/bugs.cfg)")
		data.WriteDefaultCfg(os.Stderr)
		return
	}

	run()
}

func run() {
	prj := project.New(".")

	testRun := runProjectTests(prj)

	if err := reportAll(prj.MetadataError, prj.ToProject(), testRun); err != nil {
		log.Fatal(err.Error())
	}
}

func runnerConfig() cmd.RunnerConfig {
	if options.Uids != nil && *options.Uids != "" {
		cfg, err := cmd.DefaultGroupsRunnerConfig(*options.Uids)
		if err == nil {
			return cfg
		}
		fmt.Fprintf(os.Stderr, "UIDs error: %s", err.Error())
	}
	return cmd.DefaultRunnerConfig()
}

func runProjectTests(prj *project.Project) *results.TestRun {
	cfg := runnerConfig()

	if err, tr := prj.Test(cfg); err != nil {
		log.Fatal(err.Error())
		return nil
	} else {
		return tr
	}
}

func reportAll(
	me error,
	p *results.Project,
	tr *results.TestRun) error {

	report := &report.Report{me, p, tr}

	outModes := strings.Split(strings.Trim(*options.OutputMode, " "), ",")

	reportErrs := []string{}
	for _, m := range outModes {
		if r, ok := anonymousReporters[m]; ok {
			if err := r.Write(report); err != nil {
				reportErrs = append(reportErrs, fmt.Sprintf("%s: %s", m, err.Error()))
			}
		}
	}

	if *options.RecordMode {

		if err := recordApi(report); err != nil {
			reportErrs = append(reportErrs, fmt.Sprintf("API: %s", err.Error()))
		}
	}

	if err := recordJson(report); err != nil {
		reportErrs = append(reportErrs, fmt.Sprintf("JSON: %s", err.Error()))
	}

	if len(reportErrs) == 0 {
		return nil
	}

	return fmt.Errorf("Errors occured while writing reports:\n  %s",
		strings.Join(reportErrs, "\n  "))
}

func recordApi(tr *report.Report) error {
	reporter, err := apiReporter()

	if err == nil {
		err = reporter.Write(tr)
	}

	return err
}

func recordJson(tr *report.Report) error {
	reporter := report.AsApiReporter(&report.JsonReporter{})

	return reporter.Write(tr)
}

var anonymousReporters = map[string]report.Reporter{
	options.OutXUnit: report.XUnitReporter(),
}

func apiReporter() (report.Reporter, error) {
	if dbc, err := data.NewRestDataClient(dbConfig); err != nil {
		return nil, err
	} else {
		return report.AsApiReporter(dbc), nil
	}
}
