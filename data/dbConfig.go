package data

import (
	"bytes"
	"gopkg.in/ini.v1"
	"io"
	"os"
)

const (
	sectionApi  = "api"
	keyApiUrl   = "url"
	keyApiUid   = "uid"
	keyApiToken = "token"
)

type (
	ApiConfig struct {
		Url   string
		Uid   string
		Token string
	}
)

func defaultDbConfig() *ApiConfig {
	return &ApiConfig{
		"http://localhost:2847/",
		"",
		"",
	}
}

func LoadDbConfig() *ApiConfig {
	var cfgFile *os.File

	cfgFile, _ = os.Open("bugs.cfg")

	if cfgFile == nil {
		home, ok := os.LookupEnv("HOME")
		if ok {
			cfgFile, _ = os.Open(home + "/.bugs/bugs.cfg")
		}
	}

	if cfgFile == nil {
		cfgFile, _ = os.Open("/etc/bugs/bugs.cfg")
	}

	if cfgFile != nil {
		defer cfgFile.Close()
	}

	return readCfgFile(cfgFile)

}

func readCfgFile(cfgFile *os.File) (dbConfig *ApiConfig) {

	dbConfig = defaultDbConfig()

	if cfgFile == nil {
		return
	}

	buf := new(bytes.Buffer)
	io.Copy(buf, cfgFile)

	if cfg, err := ini.Load(buf.Bytes()); err == nil {
		if sec, err := cfg.GetSection(sectionApi); err == nil {
			if key, err := sec.GetKey(keyApiUrl); err == nil {
				dbConfig.Url = key.String()
			}
			if key, err := sec.GetKey(keyApiUid); err == nil {
				dbConfig.Uid = key.String()
			}
			if key, err := sec.GetKey(keyApiToken); err == nil {
				dbConfig.Token = key.String()
			}
		}
	}

	return
}

func WriteDefaultCfg(w io.Writer) error {
	return defaultDbConfig().writeCfgFile(w)
}

func (cfg *ApiConfig) writeCfgFile(w io.Writer) error {
	// NOTE do not use on "live" config (secret leak vuln)
	iniCfg := ini.Empty()

	if apiSec, err := iniCfg.NewSection(sectionApi); err == nil {
		apiSec.NewKey(keyApiUrl, cfg.Url)
		apiSec.NewKey(keyApiUid, cfg.Uid)
		apiSec.NewKey(keyApiToken, cfg.Token)
	}

	_, err := iniCfg.WriteTo(w)

	return err
}
