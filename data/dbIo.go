package data

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/Epitech/bugs/results"
)

type (
	DataClient struct {
		*http.Client

		api *ApiConfig
	}
)

func NewRestDataClient(cfg *ApiConfig) (dc *DataClient, err error) {
	client := http.Client{Timeout: time.Duration(60) * time.Second}

	return &DataClient{&client, cfg}, nil
}

func expectHttp2xx(url string, resp *http.Response, err error) error {
	if err != nil {
		return err
	}
	if resp.StatusCode/100 != 2 {
		return errors.New(fmt.Sprintf("API request to %s returned %s", url, resp.Status))
	}
	return nil
}

func (c *DataClient) doPost(url string, json []byte) error {
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(json))
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")
	req.SetBasicAuth(c.api.Uid, c.api.Token)
	resp, err := c.Do(req)
	return expectHttp2xx(url, resp, err)
}

func (c *DataClient) RequireMetadata() bool {
	return true
}

func (c *DataClient) WriteProject(project *results.Project) error {
	if project.Module == nil {
		return errors.New("Missing module on project.")
	}

	json, err := json.Marshal(project)
	if err != nil {
		return err
	}

	url := fmt.Sprintf(
		"%s/modules/%s/projects",
		strings.TrimSuffix(c.api.Url, "/"),
		project.Module.Code,
	)

	// fmt.Printf("POST of project to %s:\n%s\n\n", url, json)

	return c.doPost(url, json)
}

func (c *DataClient) WriteTestRun(testRun *results.TestRun) error {

	if len(testRun.Instance.ModuleCode) == 0 {
		return errors.New("Missing module code on test run.")
	}

	if len(testRun.Instance.ProjectSlug) == 0 {
		return errors.New("Missing project slug on test run.")
	}

	json, err := json.Marshal(testRun)

	if err != nil {
		return err
	}

	url := fmt.Sprintf(
		"%s/testRuns",
		strings.TrimSuffix(c.api.Url, "/"),
	)

	// fmt.Printf("POST of testRun to %s:\n%s\n\n", url, json)

	return c.doPost(url, json)
}
