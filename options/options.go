package options

import (
	"errors"
	"flag"
	"fmt"
	"strings"
)

const (
	ArgOut      = "out"
	ArgRec      = "rec"
	ArgUids     = "uids"
	ArgSlug     = "slug"
	ArgModule   = "module"
	ArgInstance = "inst"
	ArgCity     = "city"
	ArgYear     = "year"
	ArgType     = "type"
	ArgLogins   = "login"

	RecApi = "api"

	OutXUnit = "xunit"
)

var (
	BugsFlags = flag.NewFlagSet("bugs", flag.PanicOnError)
	slug      = BugsFlags.String(ArgSlug, "", "Project slug")
	module    = BugsFlags.String(ArgModule, "", "Module code")
	instance  = BugsFlags.String(ArgInstance, "", "Instance code")
	login     = BugsFlags.String(ArgLogins, "", "Student login(s), comma-separated")
	city      = BugsFlags.String(ArgCity, "", "Student city")
	year      = BugsFlags.Int(ArgYear, 0, "Year")
	runType   = BugsFlags.String(ArgType, "git", "Test run type, e.g. \"git\", \"delivery\"")
	Uids      = BugsFlags.String(ArgUids, "", "UIDs to run as (testrunnerUid,studentUid)")
	Cfg       = BugsFlags.Bool("cfg", false, "Show default config and quit")
	Ver       = BugsFlags.Bool("version", false, "Show version and quit")
	Help      = BugsFlags.Bool("help", false, "Show this help message")

	OutputMode = BugsFlags.String(
		ArgOut,
		"",
		fmt.Sprintf(
			"Output format (combinable, comma-separated): %s",
			strings.Join([]string{OutXUnit}, ", "),
		),
	)

	RecordMode = BugsFlags.Bool(
		ArgRec,
		false,
		fmt.Sprintf("Record mode. Requires args %s.",
			strings.Join([]string{
				ArgModule,
				ArgSlug,
				ArgYear,
				ArgInstance,
				ArgCity,
				ArgLogins,
				ArgType,
			}, ", ")))
)

type (
	TestRunMetadata struct {
		Slug         string
		Module       string
		InstanceCode string
		City         string
		Year         int
		Logins       []string
		Type         string
	}
)

func GetMetadata() (*TestRunMetadata, error) {
	if err := paramCheck(checkApiTestRun()); err != nil {
		return nil, err
	}

	var logins []string
	if len(*login) == 0 {
		logins = []string{}
	} else {
		logins = strings.Split(*login, ",")
	}

	return &TestRunMetadata{
		*slug,
		*module,
		*instance,
		*city,
		*year,
		logins,
		*runType,
	}, nil
}

func checkApiTestRun() []string {
	missingParams := []string{}

	if len(*module) == 0 {
		missingParams = append(missingParams, ArgModule)
	}

	if len(*city) == 0 {
		missingParams = append(missingParams, ArgCity)
	}

	if len(*login) == 0 {
		missingParams = append(missingParams, ArgLogins)
	}

	if len(*slug) == 0 {
		missingParams = append(missingParams, ArgSlug)
	}

	if len(*instance) == 0 {
		missingParams = append(missingParams, ArgInstance)
	}

	if *year == 0 {
		missingParams = append(missingParams, ArgYear)
	}

	if len(*runType) == 0 {
		missingParams = append(missingParams, ArgType)
	}

	return missingParams
}

func paramCheck(missingParams []string) error {
	if len(missingParams) == 0 {
		return nil
	}

	return errors.New(fmt.Sprintf("Missing parameters for -rec api: %s",
		strings.Join(missingParams, ", ")))
}
